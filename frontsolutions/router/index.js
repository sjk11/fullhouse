import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

// The meta data for your routes
const metadata = require('./meta.json')

// Function to create routes
// Is default lazy but can be changed
function route (path, view) {
  let meta = metadata[path] || {
    "title": "",
    "description": "",
    "keywords": ""
  }
  return {
    path: path,
    name: view,
    component: resolve => import(`pages/${view}View.vue`).then(resolve)
  }
}

Vue.use(Router)
Vue.use(Meta)

export function createRouter () {
    const router = new Router({
      base: __dirname,
      mode: 'history',
      scrollBehavior: () => ({ y: 0 }),
      routes: [
        route('/', 'Home'),
        route('/about', 'About'),
        route('/entity/:slug', 'Entity'),
        route('/likes', 'Likes'),
        route('/rent', 'Rent'),
        route('/page/:slug', 'Page'),
        // Global redirect for 404
        { path: '*', redirect: '/' }
      ]
    })

    // Send a pageview to Google Analytics
    router.beforeEach((to, from, next) => {
        if (typeof ga !== 'undefined') {
            ga('set', 'page', to.path)
            ga('send', 'pageview')
        }
        next()
    })

    return router
}
