const apiUrls = {
    USER: {
        LOGIN: '/api/v1/users/token/',
        LOGOUT: '/api/v1/users/logout/',
        REGISTER: '/api/v1/users/registration/',
        CURRENT: '/api/v1/users/current/',
    },
    SETTINGS: {
        BASE: '/api/v1/settings/',
    },
    CONTENT: {
        MENUES: '/api/v1/content/menues/',
        PAGE_DETAIL: slug => `/api/v1/content/page/${slug}/`,
    },
    CLAIM: {
        ADD_SELECTION: '/api/v1/claim/add/selection/',
        ADD_OWNER: '/api/v1/claim/add/owner/',
    },
    CATALOG: {
        ENTITY_LIST: '/api/v1/catalog/entities/',
        ENTITY_AUTOCOMPLETE: '/api/v1/catalog/entity_autocomplete/',
        ENTITY_DETAIL: slug => `/api/v1/catalog/entity/${slug}/`,
        FILTERS: '/api/v1/catalog/filters/',
    },
    REVIEW: {
        SITE: '/api/v1/review/site/',
        ENTITY_GET: (entityId) => `/api/v1/review/add/entity/${entityId}/`,
    },
}
export default apiUrls

