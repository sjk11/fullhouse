function Ceramic3DPano(paramsGlob) {
  //var PANORAMA_COUNTER = 0;
  function Ceramic3DPanorama(c) {

  //var XXX = PANORAMA_COUNTER;
  //PANORAMA_COUNTER ++;

  var canvas = c;
  if (typeof canvas === "string") {
    canvas = document.getElementById(canvas);
  }
  var gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");

  var perc;
  var uperc, lapos, rectBuf, loadProg;
  var loaded;
  var width, height;

  var cubeTex, prog, uvm, upm, utex, uYsign, apos, buf;
  var invalidate;
  var Ysign;

  if (!FloatArr) {
    var FloatArr = (typeof Float32Array !== 'undefined') ? Float32Array : Array;
  }
  var oldZoom;
  var zoom;
  var proj = new FloatArr(16), view = new FloatArr(9);
  var rotationSpeed = Math.PI / 300.0;
  var pimul2 = 2 * Math.PI;
  var pidiv2 = Math.PI / 2;
  var heading, tilt;
  var mousedown, shiftx, shifty;

  function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(prog);
    gl.bindBuffer(gl.ARRAY_BUFFER, buf);
    gl.vertexAttribPointer(apos, 3, gl.FLOAT, false, 0, 0);
    gl.uniformMatrix3fv(uvm, false, view);
    gl.uniformMatrix4fv(upm, false, proj);
    gl.uniform1f(uYsign, Ysign);
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeTex);
    gl.uniform1i(utex, 0);
    gl.drawArrays(gl.TRIANGLES, 0, 6 * 2 * 3);
  }

  function setFrustum(l, r, b, t, n, f) {
    var rl = 1 / (r - l);
    var tb = 1 / (t - b);
    var nf = 1 / (n - f);
    proj[0] = (n * 2) * rl;
    proj[1] = 0;
    proj[2] = 0;
    proj[3] = 0;
    proj[4] = 0;
    proj[5] = (n * 2) * tb;
    proj[6] = 0;
    proj[7] = 0;
    proj[8] = (l + r) * rl;
    proj[9] = (b + t) * tb;
    proj[10] = (n + f) * nf;
    proj[11] = -1;
    proj[12] = 0;
    proj[13] = 0;
    proj[14] = (n * f * 2) * nf;
    proj[15] = 0;
  }

  function setPerspective(w, h, fovy, n, f) {
    var aspect = w / h;
    var invFocalLen = Math.tan(fovy / 2);
    var t = n * invFocalLen;
    var r = t * aspect;
    setFrustum(-r, r, -t, t, n, f);
  }

  function computeMatrices() {
    var ch = Math.cos(heading);
    var sh = Math.sin(heading);
    var ct = Math.cos(tilt);
    var st = Math.sin(tilt);
    var chct = ch * ct;
    var shct = sh * ct;
    var chst = ch * st;
    var shst = sh * st;
    view[0] = ch;
    view[1] = shst;
    view[2] = -shct;
    view[3] = 0;
    view[4] = ct;
    view[5] = st;
    view[6] = sh;
    view[7] = -chst;
    view[8] = chct;

    /*  direction[0] = shct;
      direction[1] = -st;
      direction[2] = -chct;*/
  }

  function rotate(sx, sy) {
    heading = heading + sx * rotationSpeed;
    tilt = tilt + sy * rotationSpeed;
    if (heading > pimul2)
      heading = heading - pimul2;
    else
      if (heading <= 0.0)
        heading = heading + pimul2;
    if (tilt > pidiv2)
      tilt = pidiv2;
    else
      if (tilt < -pidiv2)
        tilt = -pidiv2;
  }

  function resizeCanvas(w, h) {
    //console.log("resize: " + w + " " + h);
    width = w;
    height = h;
    canvas.width = w;
    canvas.height = h;
    gl.viewport(0, 0, w, h);
    invalidate = true;
    mousedown = false;
    shiftx = 0;
    shifty = 0;
  }

  function tick() {
    if (!loaded)
      return;
    //console.log("tick " + XXX + " w:" + canvas.scrollWidth + " " + width + " h:" + canvas.scrollHeight + " " + height);
    if (canvas.scrollWidth != width || canvas.scrollHeight != height) {
      //console.log("resize " + XXX);
      resizeCanvas(canvas.scrollWidth, canvas.scrollHeight);
      oldZoom = zoom - 1;
    }
    if (mousedown && (shiftx != 0 || shifty != 0)) {
      //console.log(XXX + " " + shiftx + " " + shifty);
      rotate(shiftx, shifty);
      shiftx = 0;
      shifty = 0;
      invalidate = true;
    }
    if (oldZoom != zoom) {
      oldZoom = zoom;
      invalidate = true;
      //console.log("sp " + XXX);
      setPerspective(width, height, Math.PI / 3 - zoom / 10, 0.05, 10);
    }
    if (invalidate) {
      invalidate = false;
      computeMatrices();
      draw();
    }
  }

  function getCube() {
    var cube = new FloatArr(6 * 2 * 3 * 3);
    var arr = new FloatArr(3);
    var vec = new FloatArr(3);
    var vertInd = 0;
    for (var faceInd = 0; faceInd < 3; faceInd++) {
      arr[0] = faceInd;
      arr[1] = (faceInd + 1) % 3;
      arr[2] = (faceInd + 2) % 3;
      for (var i = -1; i < 2; i += 2) {
        var counter = 0;
        for (var j = -1; j < 2; j += 2) {
          for (var k = -1; k < 2; k+=2) {
            vec[arr[0]] = i;
            vec[arr[1]] = j;
            vec[arr[2]] = k;
            for (var l = 0; l < 3; l++)
              cube[vertInd * 3 + l] = vec[l];
            if (counter == 1 || counter == 2)
              for (var l = 0; l < 3; l++)
                cube[(vertInd + 2) * 3 + l] = vec[l];
            if (counter == 2)
              vertInd = vertInd + 2;
            vertInd++;
            counter++;
          }
        }
      }
    }
    return cube;
  }

  function compileProg(vsSource, psSource) {
    var vs = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vs, vsSource);
    gl.compileShader(vs);
    if (!gl.getShaderParameter(vs, gl.COMPILE_STATUS)) {
      console.error(gl.getShaderInfoLog(vs));
      return;
    }
    var ps = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(ps, psSource);
    gl.compileShader(ps);
    if (!gl.getShaderParameter(ps, gl.COMPILE_STATUS)) {
      console.error(gl.getShaderInfoLog(ps));
      return;
    }
    var prog = gl.createProgram();
    gl.attachShader(prog, vs);
    gl.attachShader(prog, ps);
    gl.linkProgram(prog);
    if (!gl.getProgramParameter(prog, gl.LINK_STATUS)) {
      console.error(gl.getProgramInfoLog(prog));
      return;
    }
    return prog;
  }

  function initRender() {
    var vsSource = "uniform mat3 vm; uniform mat4 pm; attribute vec3 a; varying vec3 v; void main() {v = a; gl_Position = pm * vec4(vm * a, 1.0);}";
    var psSource = "precision mediump float; uniform samplerCube t; uniform float Ysign; varying vec3 v; void main() {gl_FragColor = textureCube(t, vec3(v.x, Ysign * v.z, -v.y));}";
    prog = compileProg(vsSource, psSource);

    uvm = gl.getUniformLocation(prog, "vm");
    upm = gl.getUniformLocation(prog, "pm");
    utex = gl.getUniformLocation(prog, "t");
    uYsign = gl.getUniformLocation(prog, "Ysign");
    apos = gl.getAttribLocation(prog, "a");
    gl.enableVertexAttribArray(apos);
    buf = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buf);
    gl.bufferData(gl.ARRAY_BUFFER, getCube(), gl.STATIC_DRAW);
  }

  function initTex(imgs) {
    var tex = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, tex);

    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    for (var i = 0; i < 6; i++)
    {
      gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, imgs[i]);
    }

    gl.generateMipmap(gl.TEXTURE_CUBE_MAP);
    if (gl.getError() == gl.NO_ERROR)
    {
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    }
    else
    {
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    }
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
    return tex;
  }

  function initEvents() {
    canvas.onmousedown = function (event) {
      if (!loaded) {
        event.preventDefault();
        return false;
      }
      if (event.which == 1)
        mousedown = true;
      event.preventDefault();
      return false;
    };
    //canvas.ontouchstart = function (event) {
    canvas.addEventListener(
      'touchstart',
      function (event) {
        if (!loaded) {
          event.preventDefault();
          return false;
        }
        mousedown = true;
        var touchobj = event.changedTouches[0];
        var x = (touchobj.pageX - canvas.offsetLeft) / 3;
        var y = (touchobj.pageY - canvas.offsetTop) / 3;
        mousex = x;
        mousey = y;
        event.preventDefault();
        return false;
      },
      false
    );
    /*canvas.onmouseup = */
    window.addEventListener(
      'mouseup',
      function (event) {
        if (!loaded)
          return false;
        if (event.which == 1)
          mousedown = false;
        return false;
      },
      false
    );
    window.addEventListener(
      'touchend',
      function (event) {
        if (!loaded) {
          event.preventDefault();
          return false;
        }
        mousedown = false;
        event.preventDefault();
        return false;
      },
      false
    );
    canvas.ontouchend = function (event) {
      event.preventDefault();
      return false;
    }
    /*canvas.onmouseout = */
    /*window.addEventListener(
      'mouseout',
      function () {
        mousedown = false;
      },
      false
    );*/
    var mousex = 0;
    var mousey = 0;
    /*canvas.onmousemove = */
    window.addEventListener(
      'mousemove',
      function (event) {
        if (!loaded)
          return false;
        var x = event.pageX - canvas.offsetLeft;
        var y = event.pageY - canvas.offsetTop;
        shiftx = x - mousex;
        shifty = y - mousey;
        mousex = x;
        mousey = y;
        tick();
        return false;
      },
      false
    );
    window.addEventListener(
      'touchmove',
      function (event) {
        if (!loaded) {
          event.preventDefault();
          return false;
        }
        var touchobj = event.changedTouches[0];
        var x = (touchobj.pageX - canvas.offsetLeft) / 3;
        var y = (touchobj.pageY - canvas.offsetTop) / 3;
        shiftx = x - mousex;
        shifty = y - mousey;
        mousex = x;
        mousey = y;
        tick();
        event.preventDefault();
        return false;
      },
      false
    );
    canvas.ontouchmove = function (event) {
      event.preventDefault();
      return false;
    }
    canvas.onwheel = function (event) {
      if (!loaded)
        return false;
      if (event.wheelDelta > 0) {
        zoom = Math.min(zoom + 1, 6);
      } else if (event.wheelDelta < 0) {
        zoom = Math.max(zoom - 1, -10);
      }
      if (event.deltaY < 0) {
        zoom = Math.min(zoom + 1, 6);
      } else if (event.deltaY > 0) {
        zoom = Math.max(zoom - 1, -10);
      }
      tick();
  //    console.log("zoom: " + zoom);
      return false;
    };
  }

  function init(imgs) {
    cubeTex = initTex(imgs);
    loaded = true;
    invalidate = false;
    tick();
  }

  function prepareData(srcs, cont, ready, progress) {
    var xhrTest = new window.XMLHttpRequest();
    var hasProgress = (xhrTest.onprogress !== undefined);
    //  hasProgress = false;
    var n = srcs.length;
    var tasksLeft = n;
    if (cont)
      tasksLeft++;
    var file = new Array(n);
    var bytes = new Array(n);
    var size = new Array(n);
    var sumBytes = 0;
    var sumSize = 0;
    var sizeUnknown = n;
    if (!hasProgress) {
      for (var ind = 0; ind < n; ind++) {
        bytes[ind] = 0;
        size[ind] = 1;
      }
      sumSize = n;
      sizeUnknown = 0;
    }
    function decTasks() {
      tasksLeft--;
      if (tasksLeft == 0)
        ready(file);
    }
    function XMLHttpRequestProgress(ind) {
      return function (e) {
        if (isNaN(size[ind]) && e.total > 0) {
          size[ind] = e.total;
          bytes[ind] = 0;
          sumSize += e.total;
          sizeUnknown--;
        }
        if (!isNaN(size[ind]) && e.loaded > bytes[ind]) {
          sumBytes += e.loaded - bytes[ind];
          bytes[ind] = e.loaded;
          if (sizeUnknown == 0)
            progress(sumBytes / sumSize);
        }
      }
    }
    function XMLHttpRequestTaskReady(ind, isImage) {
      return function () {
        if (isNaN(size[ind]))
          sizeUnknown--;
        else {
          if (size[ind] > bytes[ind]) {
            sumBytes += size[ind] - bytes[ind];
            bytes[ind] = size[ind];
            if (sizeUnknown == 0)
              progress(sumBytes / sumSize);
          }
        }
        if (this.status >= 200 && this.status < 400) {
          if (isImage) {
            var h = this.getAllResponseHeaders(),
                m = h.match( /^Content-Type\:\s*(.*?)$/mi ),
                mimeType = m[1] || 'image/png';
            var blob = new Blob([this.response], {type: mimeType});
            file[ind] = new Image();
            file[ind].onload = decTasks;
            file[ind].src = window.URL.createObjectURL(blob);
          } else {
            file[ind] = this.response;
            decTasks();
          }
        } else {
          console.error("Unable to load file " + srcs[ind] + "!");
          decTasks();
        }
      }
    }
    for (var ind = 0; ind < n; ind++) {
      var src = srcs[ind];
      var ext = src.substr(src.lastIndexOf("."));
      var isImage = (ext == ".jpg" || ext == ".bmp" || ext == ".png");
      var isBinary = (isImage || ext == ".bin");
      if (!hasProgress && isImage) {
        file[ind] = new Image();
        file[ind].onload = function () {
          sumBytes++;
          progress(sumBytes / sumSize);
          decTasks();
        };
        file[ind].src = src;
      } else {
        var request = new XMLHttpRequest();
        request.onload = XMLHttpRequestTaskReady(ind, isImage);
        if (hasProgress)
          request.onprogress = XMLHttpRequestProgress(ind);
        request.open('GET', src, true);
        if (isBinary)
          request.responseType = "arraybuffer";
        request.send();
      }
    }
    if (cont) {
      cont();
      decTasks();
    }
  }

  function beforeLoadedDraw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(loadProg);
    gl.bindBuffer(gl.ARRAY_BUFFER, rectBuf);
    gl.vertexAttribPointer(lapos, 3, gl.FLOAT, false, 0, 0);
    gl.uniform1f(uperc, perc);
    gl.drawArrays(gl.TRIANGLES, 0, 6);
  }

  function beforeLoadedTick() {
    if (loaded)
      return;
    if (canvas.scrollWidth != width || canvas.scrollHeight != height)
      resizeCanvas(canvas.scrollWidth, canvas.scrollHeight);
    if (invalidate) {
      invalidate = false;
      beforeLoadedDraw();
    }
  }

  function beforeLoadedInit() {
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    var vsSource = "attribute vec3 a; varying vec2 v; void main() {v = a.xy; gl_Position = vec4(a, 1.0);}";
    var psSource = "precision mediump float; uniform float perc; varying vec2 v; void main() {if (abs(v.x) > 0.4 || abs(v.y) > 0.05) discard;"+
                   "if ((v.x + 0.4)/0.8 < perc) gl_FragColor = vec4(0.0, 0.7, 0.0, 1.0); else gl_FragColor = vec4(0.7, 0.7, 0.7, 1.0);}";
    loadProg = compileProg(vsSource, psSource);
    uperc = gl.getUniformLocation(loadProg, "perc");
    lapos = gl.getAttribLocation(loadProg, "a");
    gl.enableVertexAttribArray(lapos);
    rectBuf = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, rectBuf);
    gl.bufferData(gl.ARRAY_BUFFER, new FloatArr([
      -1.0, -1.0, 0.0,
      -1.0,  1.0, 0.0,
       1.0, -1.0, 0.0,
       1.0,  1.0, 0.0,
      -1.0,  1.0, 0.0,
       1.0, -1.0, 0.0
    ]), gl.STATIC_DRAW);
  }

  beforeLoadedInit();
  initRender();
  initEvents();

  return {
    reload: function (params) {
      //console.log("Start reload" + XXX);
      perc = 0.0;
      loaded = false;
      invalidate = true;
      Ysign = (params.cameraVersion == 1) ? 1.0 : -1.0;
      oldZoom = -100;
      zoom = 0;
      heading = 0.0;
      tilt = 0.0;
      mousedown = false;
      shiftx = 0;
      shifty = 0;
      gl.deleteTexture(cubeTex);
      beforeLoadedTick();
      prepareData(params.panorams[0], null, init, function (p) {perc = p; invalidate = true; beforeLoadedTick();});
    },
    redraw: function () {
      tick();
    }
  }
  }
  (function go() {
    var Panorama;
    Panorama = paramsGlob.canvas;
    if (typeof Panorama === "string") {
      Panorama = document.getElementById(Panorama);
    }
    //Panorama.style.position = 'absolute';
    Panorama.c3dpano = Ceramic3DPanorama(Panorama);
    function cancelEvent() {
      return false;
    }
    Panorama.onselectstart = cancelEvent;
    Panorama.ondragstart = cancelEvent;
    function getParams(i, j){
      var out = new Object();
      out.panorams = paramsGlob.panorams;
      out.cameraVersion = paramsGlob.cameraVersion;
      return out;
    }
    Panorama.c3dpano.reload(getParams(0, 0));
  })();
}
export default Ceramic3DPano