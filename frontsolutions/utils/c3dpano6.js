//var PANORAMA_COUNTER = 0;
function Ceramic3DPanorama(c) {

  //var XXX = PANORAMA_COUNTER;
  //PANORAMA_COUNTER ++;

  var canvas = c;
  if (typeof canvas === "string") {
    canvas = document.getElementById(canvas);
  }
  var gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");

  var perc;
  var uperc, lapos, rectBuf, loadProg;
  var loaded;
  var width, height;

  var cubeTex, prog, uvm, upm, utex, uYsign, apos, buf;
  var invalidate;
  var Ysign;
  var Ysign;

  if (!FloatArr) {
    var FloatArr = (typeof Float32Array !== 'undefined') ? Float32Array : Array;
  }
  var oldZoom;
  var zoom;
  var proj = new FloatArr(16), view = new FloatArr(9);
  var invPRTMatr = new FloatArr(16);
  var invPMatr = new FloatArr(16);
  var rotationSpeed = Math.PI / 300.0;
  var pimul2 = 2 * Math.PI;
  var pidiv2 = Math.PI / 2;
  var heading, tilt;
  var mousedown, shiftx, shifty;

  function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(prog);
    gl.bindBuffer(gl.ARRAY_BUFFER, buf);
    gl.vertexAttribPointer(apos, 2, gl.FLOAT, false, 0, 0);
    //gl.uniformMatrix3fv(uvm, false, view);
    //invPRTMatr = [-0.74895578622818, 0.662619948387146, 0, -0.00578223587945104, -0.0065356302075088, 0.999961912631989, 0.662594735622406, 0.748927295207977, 0.00872632302343845];  
    //invPRTMatr = [-0.817341804504394, 0.723122775554657, 0, 0, -0.0059033608995378, -0.006672537419945, 1.02090895175934, 0, 0, 0, 0, -49.9500007629394, -0.662594735622406, -0.748927295207977, -0.00872632302343845, 50.0500030517578];
    gl.uniformMatrix4fv(upm, false, invPRTMatr);
    gl.uniform1f(uYsign, Ysign);
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeTex);
    gl.uniform1i(utex, 0);
    gl.drawArrays(gl.TRIANGLES, 0, 3);
  }

  function coord4(i,j){
    return i * 4 + j; 
  }
  function rxmatmul(matr, theta) {
    var matr2 = new FloatArr(16);
    matr2 = matr.slice(0, 16);
    for (var j = 0; j < 4; j++){
      matr2[coord4(1, j)] = Math.cos(theta) * matr[coord4(1, j)] - Math.sin(theta)*matr[coord4(2, j)];
      matr2[coord4(2, j)] = Math.sin(theta) * matr[coord4(1, j)] + Math.cos(theta)*matr[coord4(2, j)];
    }  
    return matr2;
  }

  function rzmatmul(matr, theta) {
    var matr2 = new FloatArr(16);
    matr2 = matr.slice(0, 16);
    for (var j = 0; j < 4; j++){
      matr2[coord4(0, j)] = Math.cos(theta) * matr[coord4(0, j)] - Math.sin(theta)*matr[coord4(1, j)];
      matr2[coord4(1, j)] = Math.sin(theta) * matr[coord4(0, j)] + Math.cos(theta)*matr[coord4(1, j)];
    }
    return matr2;
  }

  function getInvInvView(invView) {
    var matr2 = new FloatArr(16);
    for (var i = 0; i < 3; i++)
      for (var j = 0; j < 3; j++)
        matr2[coord4(i, j)] = invView[coord4(j, i)];
    matr2[coord4(3, 0)] = 0;
    matr2[coord4(3, 1)] = 0;
    matr2[coord4(3, 2)] = 0;
    matr2[coord4(3, 3)] = 1;
    matr2[coord4(0, 3)] = -(matr2[coord4(0, 0)]*invView[coord4(0, 3)] + matr2[coord4(0, 1)]*invView[coord4(1, 3)] + matr2[coord4(0, 2)]*invView[coord4(2, 3)]);
    matr2[coord4(1, 3)] = -(matr2[coord4(1, 0)]*invView[coord4(0, 3)] + matr2[coord4(1, 1)]*invView[coord4(1, 3)] + matr2[coord4(1, 2)]*invView[coord4(2, 3)]);
    matr2[coord4(2, 3)] = -(matr2[coord4(2, 0)]*invView[coord4(0, 3)] + matr2[coord4(2, 1)]*invView[coord4(1, 3)] + matr2[coord4(2, 2)]*invView[coord4(2, 3)]);
    return matr2;
  }


  function matMul(A, B) {
    var matr2 = new FloatArr(16);
    for (var i = 0; i < 4; i++)
      for (var j = 0; j < 4; j++)
        matr2[coord4(i, j)] = 0;
    for (var i = 0; i < 4; i++)
      for (var j = 0; j < 4; j++)
        for (var k = 0; k < 4; k++)
          matr2[coord4(i, j)] = matr2[coord4(i, j)] + A[coord4(i, k)] * B[coord4(k, j)];
    return matr2;
  }

  function setInvPerspectiveMatr(l, r, b, t, n, f) {
    invPMatr[coord4(0, 0)] = (r - l) / 2 / n;
    invPMatr[coord4(0, 1)] = 0;
    invPMatr[coord4(0, 2)] = 0;
    invPMatr[coord4(0, 3)] = (r + l) / 2 / n;
    invPMatr[coord4(1, 0)] = 0;
    invPMatr[coord4(1, 1)] = (t - b) / 2 / n;
    invPMatr[coord4(1, 2)] = 0;
    invPMatr[coord4(1, 3)] = (t + b)/ 2 / n;
    invPMatr[coord4(2, 0)] = 0;
    invPMatr[coord4(2, 1)] = 0;
    invPMatr[coord4(2, 2)] = 0;
    invPMatr[coord4(2, 3)] = -1;
    invPMatr[coord4(3, 0)] = 0;
    invPMatr[coord4(3, 1)] = 0;
    invPMatr[coord4(3, 2)] = (n - f) / 2 / n / f;
    invPMatr[coord4(3, 3)] = (n + f) / 2 / n / f;  
  }

  function setPerspective(w, h, fovy, n, f) {
    var invFocalLength = Math.tan(fovy / 2);
    var aspectW = 1.0;
    var aspectH = 1.0;
    if (w > h)
      aspectH = h / w;
    else
      aspectW = w / h;
    var t = n * invFocalLength * aspectH;
    var b = -t;
    var r = n * invFocalLength * aspectW;
    var l = -r;
    setInvPerspectiveMatr(l, r, b, t, n, f);
  }

  function transposeMatr(matr){
    var matr2 = new FloatArr(16);
    for (var i = 0; i < 4; i++)
      for (var j = 0; j < 4; j++)
        matr2[coord4(j, i)]=matr[coord4(i, j)];
    return matr2;
  }

  function computeMatr(){
    var invView = new FloatArr(16);
    invView = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
    invView = rxmatmul(invView, Math.PI / 2 + tilt * Math.PI / 180);
    invView = rzmatmul(invView, heading * Math.PI / 180);
    
    // console.log('invView', [0.6427877373664246, -0.13302238414175144, -0.7544063692789164, 0, -0.7660443359828153, -0.11161907125370601, -0.6330223204920248, -0, 0, 0.9848077113069863, -0.17364841418883037, 0, 0, 0, 0, 1])
    invPRTMatr = matMul(invView, invPMatr);
    invPRTMatr = transposeMatr(invPRTMatr);
  }

  function rotate(sx, sy){
    heading = heading - sx / 2;
    tilt = tilt + sy / 2;
    if (heading > 360)
      heading = heading - 360;
    else
      if (heading <= 0)
        heading = heading + 360;
    if (tilt > 90)
      tilt = 90;
    else
      if (tilt < -90)
        tilt = -90;
  }
  /*function rotate(sx, sy) {
    heading = heading + sx * rotationSpeed;
    tilt = tilt + sy * rotationSpeed;
    if (heading > pimul2)
      heading = heading - pimul2;
    else
      if (heading <= 0.0)
        heading = heading + pimul2;
    if (tilt > pidiv2)
      tilt = pidiv2;
    else
      if (tilt < -pidiv2)
        tilt = -pidiv2;
  }*/

  function resizeCanvas(w, h) {
    //console.log("resize: " + w + " " + h);
    width = w;
    height = h;
    canvas.width = w;
    canvas.height = h;
    gl.viewport(0, 0, w, h);
    invalidate = true;
    mousedown = false;
    shiftx = 0;
    shifty = 0;
  }

  function tick() {
    if (!loaded)
      return;
    //console.log("tick " + XXX + " w:" + canvas.scrollWidth + " " + width + " h:" + canvas.scrollHeight + " " + height);
    if (canvas.scrollWidth != width || canvas.scrollHeight != height) {
      //console.log("resize " + XXX);
      resizeCanvas(canvas.scrollWidth, canvas.scrollHeight);
      oldZoom = zoom - 1;
    }
    if (mousedown && (shiftx != 0 || shifty != 0)) {
      //console.log(XXX + " " + shiftx + " " + shifty);
      rotate(shiftx, shifty);
      shiftx = 0;
      shifty = 0;
      invalidate = true;
    }
    if (oldZoom != zoom) {
      oldZoom = zoom;
      invalidate = true;
      //console.log("sp " + XXX);
      setPerspective(width, height, Math.PI *95 / 180 - zoom / 10, 0.01, 10);
      //setPerspective(760, 711, Math.PI *95 / 180 - zoom / 10, 0.01, 10);
    }
    if (invalidate) {
      invalidate = false;
      computeMatr();
      draw();
    }
  }

  function getCube() {
    var cube = new FloatArr(6);
    cube[0] = -1.0; cube[1] = -1.0; cube[2] = 3.0; cube[3] = -1.0; cube[4] = -1.0; cube[5] = 3.0;
    return cube;
  }

  function compileProg(vsSource, psSource) {
    var vs = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vs, vsSource);
    gl.compileShader(vs);
    if (!gl.getShaderParameter(vs, gl.COMPILE_STATUS)) {
      console.error(gl.getShaderInfoLog(vs));
      return;
    }
    var ps = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(ps, psSource);
    gl.compileShader(ps);
    if (!gl.getShaderParameter(ps, gl.COMPILE_STATUS)) {
      console.error(gl.getShaderInfoLog(ps));
      return;
    }
    var prog = gl.createProgram();
    gl.attachShader(prog, vs);
    gl.attachShader(prog, ps);
    gl.linkProgram(prog);
    if (!gl.getProgramParameter(prog, gl.LINK_STATUS)) {
      console.error(gl.getProgramInfoLog(prog));
      return;
    }
    return prog;
  }

  function initRender() {
    var vsSource = "uniform mat4 invMatr;   attribute vec2 a;   varying vec4 dir;   void main()    {     gl_Position = vec4(a.x, a.y, 0.0, 1.0);     vec4 v1 = invMatr * vec4(a.x, -a.y, -1.0, 1.0);     dir = v1 / v1.w;   }";
    var psSource = "precision mediump float;   uniform samplerCube t;   uniform float Ysign;   varying vec4 dir;   void main()   {     vec3 texCoord = vec3(dir.x, -Ysign * dir.y, dir.z);     gl_FragColor = textureCube(t, texCoord);   }";
    prog = compileProg(vsSource, psSource);

    //uvm = gl.getUniformLocation(prog, "vm");
    upm = gl.getUniformLocation(prog, "invMatr");
    utex = gl.getUniformLocation(prog, "t");
    uYsign = gl.getUniformLocation(prog, "Ysign");
    apos = gl.getAttribLocation(prog, "a");
    gl.enableVertexAttribArray(apos);
    buf = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buf);
    gl.bufferData(gl.ARRAY_BUFFER, getCube(), gl.STATIC_DRAW);
  }

  function initTex(imgs) {
    var tex = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, tex);

    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    for (var i = 0; i < 6; i++)
    {
      gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, imgs[i]);
    }

    gl.generateMipmap(gl.TEXTURE_CUBE_MAP);
    if (gl.getError() == gl.NO_ERROR)
    {
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    }
    else
    {
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    }
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
    return tex;
  }

  function initEvents() {
    canvas.onmousedown = function (event) {
      if (!loaded) {
        event.preventDefault();
        return false;
      }
      if (event.which == 1)
        mousedown = true;
      event.preventDefault();
      return false;
    };
    //canvas.ontouchstart = function (event) {
    canvas.addEventListener(
      'touchstart',
      function (event) {
        if (!loaded) {
          event.preventDefault();
          return false;
        }
        mousedown = true;
        var touchobj = event.changedTouches[0];
        var x = (touchobj.pageX - canvas.offsetLeft) / 3;
        var y = (touchobj.pageY - canvas.offsetTop) / 3;
        mousex = x;
        mousey = y;
        event.preventDefault();
        return false;
      },
      false
    );
    /*canvas.onmouseup = */
    window.addEventListener(
      'mouseup',
      function (event) {
        if (!loaded)
          return false;
        if (event.which == 1)
          mousedown = false;
        return false;
      },
      false
    );
    window.addEventListener(
      'touchend',
      function (event) {
        if (!loaded) {
          event.preventDefault();
          return false;
        }
        mousedown = false;
        event.preventDefault();
        return false;
      },
      false
    );
    canvas.ontouchend = function (event) {
      event.preventDefault();
      return false;
    }
    /*canvas.onmouseout = */
    /*window.addEventListener(
      'mouseout',
      function () {
        mousedown = false;
      },
      false
    );*/
    var mousex = 0;
    var mousey = 0;
    /*canvas.onmousemove = */
    window.addEventListener(
      'mousemove',
      function (event) {
        if (!loaded)
          return false;
        var x = event.pageX - canvas.offsetLeft;
        var y = event.pageY - canvas.offsetTop;
        shiftx = x - mousex;
        shifty = y - mousey;
        mousex = x;
        mousey = y;
        tick();
        return false;
      },
      false
    );
    window.addEventListener(
      'touchmove',
      function (event) {
        if (!loaded) {
          event.preventDefault();
          return false;
        }
        var touchobj = event.changedTouches[0];
        var x = (touchobj.pageX - canvas.offsetLeft) / 3;
        var y = (touchobj.pageY - canvas.offsetTop) / 3;
        shiftx = x - mousex;
        shifty = y - mousey;
        mousex = x;
        mousey = y;
        tick();
        event.preventDefault();
        return false;
      },
      false
    );
    canvas.ontouchmove = function (event) {
      event.preventDefault();
      return false;
    }
    canvas.onwheel = function (event) {
      if (!loaded)
        return false;
      if (event.wheelDelta > 0) {
        zoom = Math.min(zoom + 1, 6);
      } else if (event.wheelDelta < 0) {
        zoom = Math.max(zoom - 1, -10);
      }
      if (event.deltaY < 0) {
        zoom = Math.min(zoom + 1, 6);
      } else if (event.deltaY > 0) {
        zoom = Math.max(zoom - 1, -10);
      }
      tick();
  //    console.log("zoom: " + zoom);
      return false;
    };
  }

  function init(imgs) {
    cubeTex = initTex(imgs);
    loaded = true;
    invalidate = false;
    tick();
  }

  function prepareData(srcs, cont, ready, progress) {
    var xhrTest = new window.XMLHttpRequest();
    var hasProgress = (xhrTest.onprogress !== undefined);
  //  hasProgress = false;
    var n = srcs.length;
    var tasksLeft = n;
    if (cont)
      tasksLeft++;
    var file = new Array(n);
    var bytes = new Array(n);
    var size = new Array(n);
    var sumBytes = 0;
    var sumSize = 0;
    var sizeUnknown = n;
    if (!hasProgress) {
      for (var ind = 0; ind < n; ind++) {
        bytes[ind] = 0;
        size[ind] = 1;
      }
      sumSize = n;
      sizeUnknown = 0;
    }
    function decTasks() {
      tasksLeft--;
      if (tasksLeft == 0)
        ready(file);
    }
    function XMLHttpRequestProgress(ind) {
      return function (e) {
        if (isNaN(size[ind]) && e.total > 0) {
          size[ind] = e.total;
          bytes[ind] = 0;
          sumSize += e.total;
          sizeUnknown--;
        }
        if (!isNaN(size[ind]) && e.loaded > bytes[ind]) {
          sumBytes += e.loaded - bytes[ind];
          bytes[ind] = e.loaded;
          if (sizeUnknown == 0)
            progress(sumBytes / sumSize);
        }
      }
    }
    function XMLHttpRequestTaskReady(ind, isImage) {
      return function () {
        if (isNaN(size[ind]))
          sizeUnknown--;
        else {
          if (size[ind] > bytes[ind]) {
            sumBytes += size[ind] - bytes[ind];
            bytes[ind] = size[ind];
            if (sizeUnknown == 0)
              progress(sumBytes / sumSize);
          }
        }
        if (this.status >= 200 && this.status < 400) {
          if (isImage) {
            var h = this.getAllResponseHeaders(),
                m = h.match( /^Content-Type\:\s*(.*?)$/mi ),
                mimeType = m[1] || 'image/png';
            var blob = new Blob([this.response], {type: mimeType});
            file[ind] = new Image();
            file[ind].onload = decTasks;
            file[ind].src = window.URL.createObjectURL(blob);
          } else {
            file[ind] = this.response;
            decTasks();
          }
        } else {
          console.error("Unable to load file " + srcs[ind] + "!");
          decTasks();
        }
      }
    }
    for (var ind = 0; ind < n; ind++) {
      var src = srcs[ind];
      var ext = src.substr(src.lastIndexOf("."));
      var isImage = (ext == ".jpg" || ext == ".bmp" || ext == ".png");
      var isBinary = (isImage || ext == ".bin");
      if (!hasProgress && isImage) {
        file[ind] = new Image();
        file[ind].onload = function () {
          sumBytes++;
          progress(sumBytes / sumSize);
          decTasks();
        };
        file[ind].src = src;
      } else {
        var request = new XMLHttpRequest();
        request.onload = XMLHttpRequestTaskReady(ind, isImage);
        if (hasProgress)
          request.onprogress = XMLHttpRequestProgress(ind);
        request.open('GET', src, true);
        if (isBinary)
          request.responseType = "arraybuffer";
        request.send();
      }
    }
    if (cont) {
      cont();
      decTasks();
    }
  }

  function beforeLoadedDraw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(loadProg);
    gl.bindBuffer(gl.ARRAY_BUFFER, rectBuf);
    gl.vertexAttribPointer(lapos, 3, gl.FLOAT, false, 0, 0);
    gl.uniform1f(uperc, perc);
    gl.drawArrays(gl.TRIANGLES, 0, 6);
  }

  function beforeLoadedTick() {
    if (loaded)
      return;
    if (canvas.scrollWidth != width || canvas.scrollHeight != height)
      resizeCanvas(canvas.scrollWidth, canvas.scrollHeight);
    if (invalidate) {
      invalidate = false;
      beforeLoadedDraw();
    }
  }

  function beforeLoadedInit() {
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    var vsSource = "attribute vec3 a; varying vec2 v; void main() {v = a.xy; gl_Position = vec4(a, 1.0);}";
    var psSource = "precision mediump float; uniform float perc; varying vec2 v; void main() {if (abs(v.x) > 0.4 || abs(v.y) > 0.05) discard;"+
                   "if ((v.x + 0.4)/0.8 < perc) gl_FragColor = vec4(0.0, 0.7, 0.0, 1.0); else gl_FragColor = vec4(0.7, 0.7, 0.7, 1.0);}";
    loadProg = compileProg(vsSource, psSource);
    uperc = gl.getUniformLocation(loadProg, "perc");
    lapos = gl.getAttribLocation(loadProg, "a");
    gl.enableVertexAttribArray(lapos);
    rectBuf = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, rectBuf);
    gl.bufferData(gl.ARRAY_BUFFER, new FloatArr([
      -1.0, -1.0, 0.0,
      -1.0,  1.0, 0.0,
       1.0, -1.0, 0.0,
       1.0,  1.0, 0.0,
      -1.0,  1.0, 0.0,
       1.0, -1.0, 0.0
    ]), gl.STATIC_DRAW);
  }

  beforeLoadedInit();
  initRender();
  initEvents();

  return {
    reload: function (params) {
      console.log("Start reload ======", params);
      perc = 0.0;
      loaded = false;
      invalidate = true;
      Ysign = (params.cameraVersion == 1) ? 1.0 : -1.0;
      oldZoom = -100;
      zoom = 0;
      if (Math.abs(params.dir[2]) < 0.5)
        heading = 180 + Math.atan2(params.dir[0], -params.dir[1]) * 180 / Math.PI;
      else
      if (params.dir[2] >= 0.5)
        heading = 180 + Math.atan2(-params.up[0], params.up[1]) * 180 / Math.PI;
      else
        heading = 180 + Math.atan2(params.up[0], -params.up[1]) * 180 / Math.PI;
      tilt = -180.0 / Math.PI * Math.asin(params.dir[2]);
      mousedown = false;
      shiftx = 0;
      shifty = 0;
      gl.deleteTexture(cubeTex);
      beforeLoadedTick();
      prepareData(params.panorams[0], null, init, function (p) {perc = p; invalidate = true; beforeLoadedTick();});
    },
    redraw: function () {
      tick();
    }
  }
}
export default Ceramic3DPanorama