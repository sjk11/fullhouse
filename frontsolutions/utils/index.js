function groupBy (xs, key, unique) {
  return xs.reduce(function(rv, x) {
    if (unique) {
        rv[x[key]] = x
    } else {
        (rv[x[key]] = rv[x[key]] || []).push(x);
    }
    return rv;
  }, {});
}

function truncate(content, limit, after) {
    content = content.trim().split(' ').slice(0, limit)
    return content.join(' ') + (after ? after : '')
}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
};

function declOfNum(number, titles) {  
    let cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}
function isObject(obj) {
  return obj === Object(obj);
}
function parseParams(query, params, prefix, removeEmpty) { 
  query = Object.assign({}, query)
  params = params || {}
  prefix = prefix || ''
  removeEmpty = removeEmpty === undefined ? true : removeEmpty
  for (let k in query) {
    if ((query[k] === null || query[k] === '') && removeEmpty) {
      delete query[k]
    }
    else if (Array.isArray(query[k])) {
      if (query[k].length  || !removeEmpty) {
        params[`${prefix}${k}`] = query[k]
      }
    }
    else if (isObject(query[k])) {
      params = parseParams(query[k], params, `${k}__`, removeEmpty)
    }
    else {
      params[`${prefix}${k}`] = query[k]
    }
  }  
  return params
}


function setNestedPropertyValue(obj, fields, val, delimetr , created)
{
  created = created || false
  fields = fields.split(delimetr);

  let cur = obj,
  last = fields.pop();

  fields.forEach(function(field) {
      if ((!cur.hasOwnProperty(field) || typeof cur[field] !== 'object') && created) {
        cur[field] = {}
      }
      if (cur.hasOwnProperty(field)) {
        cur = cur[field]
      }
  })

  cur[last] = val

  return obj
}

function parseQueryParams(query, params, removeEmpty, delimetr) { 
  params = params || {}
  delimetr = delimetr || '__'
  removeEmpty = removeEmpty === undefined ? true : removeEmpty
  for (let k in query) {
    let v = query[k]
    if (removeEmpty) {
      if ((v === null || v === '') && removeEmpty) {
        delete query[k]
        continue
      }
    }
    setNestedPropertyValue(params, k, v, delimetr)
  }  
  return params
}
function stringToDate(_date,_format,_delimiter)
{
  let formatLowerCase=_format.toLowerCase();
  let formatItems=formatLowerCase.split(_delimiter);
  let dateItems=_date.split(_delimiter);
  let monthIndex=formatItems.indexOf("mm");
  let dayIndex=formatItems.indexOf("dd");
  let yearIndex=formatItems.indexOf("yyyy");
  let month=parseInt(dateItems[monthIndex]);
  month-=1;
  let formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
  return formatedDate;
}
//declOfNum(count, ['найдена', 'найдено', 'найдены']);

export {groupBy, truncate, formatMoney, declOfNum, parseParams, parseQueryParams, stringToDate}