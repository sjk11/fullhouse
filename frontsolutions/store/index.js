import Vue from 'vue'
import Vuex from 'vuex'
import apiUrls from 'router/apiUrls'
import {groupBy} from 'utils/index'
import axios from 'axios'
const qs = require('qs')

import {parseParams} from 'utils/index'
  import { format, subDays, addDays } from 'date-fns';

let _log = console.log,
  utc = new Date().toJSON().slice(0,10),
  utc2 = new Date(Date.now()+ 2 *24*60*60*1000).toJSON().slice(0,10)

Vue.use(Vuex)

export function createStore () {
  return new Vuex.Store({
    state: {
      cookieContext: {liked: []},
      currentUser: {isAuth: false},
      loadUser: false,
      appSettings: undefined,
      loadSettings: false,
      paramFilters: {
        page: 1,
        // page_size: 15,
        ordering: null,
        search: null,
        entitytype: [],
        stay_date: {'gte': format(new Date(), 'DD/MM/YYYY'), 'lte': format(addDays(new Date(), 3), 'DD/MM/YYYY')},
        bulk_man: {'gte': 0, 'lte': 100},
        price: {'gte': 0, 'lte': 100000},
        len_share: null,
        BoolAttr: [] 
      },
      breadcrumbs: [],
      menues: [],
      loadMenues: false,
      pageDataParams: {
        headerCssClass: [],
        subTitle: null,
        insubTitle: null,
        extrasubTitle: null,
        subTitleSlogan: null,
        subTitleText: null,
        showtitlecontent: null,
        showclaim: null,
        showOwnerClaim: false,
      },
      activeView: 'block',
      viewChoice: {
        block: {icon: 'fa-th'},
        list: {icon: 'fa-list'},
        map: {icon: 'fa-map-marker'},
      },
      activeSort: null,
      loadEntityData: false,
      entityData: {},
      loadFilterData: false,
      filterData: {},
      sortChoice: {
        price_workday: [
          { title: 'Цене ↗', 'value': 'price_workday'},
          { title: 'Цене ↙', 'value': '-price_workday' },
        ],
        // bulk_man: [
        //   { title: 'Количество ↗', 'value': 'bulk_man' },
        //   { title: 'Количество ↙', 'value': '-bulk_man' },
        // ],
      },
      loadDetailEntity: false,
      detailEntity: {},
      loadDetailPage: false,
      detailPage: {},
      reviews: [],
      loadReviews: false,
      snackMsgs: [], // {color, show, text}
      loadSnackMsgs: false,
    },
    actions: {
      getCurrentUser({state, commit}, usertoken) {
        if (!state.loadUser && !!state.cookieContext.usertoken) {
          commit('SET_LOAD', {param: 'loadUser', flag: true})
          return axios.get(apiUrls.USER.CURRENT).then(resp => {
            resp.data['isAuth'] = true
            commit('SET_STATE', {param: 'currentUser', value: resp.data})
          }).catch(err => {
            console.log('====err', err)
            commit('CLEAN_USER')
          }).finally( () => {
            commit('SET_LOAD', {param: 'loadUser', flag: false})
          }) 
        }
      },
      getMenues({state, commit}) {
        if (!state.loadMenues) {
          commit('SET_LOAD', {param: 'loadMenues', flag: true})
          return axios.get(apiUrls.CONTENT.MENUES).then(resp => {
            commit('SET_STATE', {param: 'menues', value: resp.data.results})
          }).catch(err => {
            console.log('====err ', err)
          }).finally( () => {
            commit('SET_LOAD', {param: 'loadMenues', flag: false})
          }) 
        }
      },
      getEntityData({state, commit}, params) {
        params = parseParams(params || {})

        if (!state.loadEntityData) {
          commit('SET_LOAD', {param: 'loadEntityData', flag: true})
          return axios.get(apiUrls.CATALOG.ENTITY_LIST, {
            params,
            paramsSerializer: dataparams => qs.stringify(dataparams, { arrayFormat: 'repeat' })
          }).then(resp => {
            commit('SET_STATE', {param: 'entityData', value: resp.data})
          }).catch(err => {
            console.log('====err ', err)
          }).finally( () => {
            commit('SET_LOAD', {param: 'loadEntityData', flag: false})
          }) 
        }
      },
      getDetailEntity({state, commit}, slug) {

        if (!state.loadDetailEntity) {
          commit('SET_LOAD', {param: 'loadDetailEntity', flag: true})
          return axios.get(apiUrls.CATALOG.ENTITY_DETAIL(slug)).then(resp => {
            commit('SET_STATE', {param: 'detailEntity', value: resp.data})
          }).catch(err => {
            console.log('====err ', err)
          }).finally( () => {
            commit('SET_LOAD', {param: 'loadDetailEntity', flag: false})
          }) 
        }
      },
      getDetailPage({state, commit}, slug) {

        if (!state.loadDetailPage) {
          commit('SET_LOAD', {param: 'loadDetailPage', flag: true})
          return axios.get(apiUrls.CONTENT.PAGE_DETAIL(slug)).then(resp => {
            commit('SET_STATE', {param: 'detailPage', value: resp.data})
          }).catch(err => {
            console.log('====err ', err)
          }).finally( () => {
            commit('SET_LOAD', {param: 'loadDetailPage', flag: false})
          }) 
        }
      },
      getFilterData({state, commit, dispatch}) {
        if (!state.loadFilterData) {
          commit('SET_LOAD', {param: 'loadFilterData', flag: true})
          return axios.get(apiUrls.CATALOG.FILTERS, {}).then(resp => {
            if(resp.data.Entity) {
              let ent_data = resp.data.Entity,
                max_price = ent_data.max_price_holyday > ent_data.max_price_workday ? ent_data.max_price_holyday : ent_data.max_price_workday,
                min_price = ent_data.min_price_holyday < ent_data.min_price_workday ? ent_data.min_price_holyday : ent_data.min_price_workday,
                paramFilters = {
                  bulk_man: {'gte': ent_data.min_bulk_man, 'lte': ent_data.max_bulk_man},
                  price: {'gte': min_price, 'lte': max_price},
                }
              resp.data.Entity['paramFilters'] = {}
              resp.data.Entity['paramFilters'] = paramFilters
              dispatch('setParamFiltersByName', paramFilters)
            }
            commit('SET_STATE', {param: 'filterData', value: resp.data})

          }).catch(err => {
            console.log('====err ', err)
          }).finally( () => {
            commit('SET_LOAD', {param: 'loadFilterData', flag: false})
          }) 
        }
      },
      logoutCurrentUser({state, commit}) {
        if (!state.loadUser) {
          commit('SET_LOAD', {param: 'loadUser', flag: true})
          return axios.post(apiUrls.USER.CURRENT).then(resp => {

          }).catch(err => {
            console.log('====err', err)
          }).finally( () => {
            commit('CLEAN_USER')
            commit('SET_LOAD', {param: 'loadUser', flag: false})
          }) 
        }
        return new Promise((resolve) => { 
        
          resolve(null)
        })
      },
      setToken({commit}, data) {
        for(const [k, v] of Object.entries(data)) {
          commit('SET_CONTEXT', {k, v})
        }
      },
      setBc({commit, state}, bc) {
        commit('SET_STATE', {param: 'breadcrumbs', value: bc})
      },
      getSettings({commit, state}){
        if (!state.loadSettings && state.appSettings === undefined ) {
          commit('SET_LOAD', {param: 'loadSettings', flag: true})
          return axios.get(apiUrls.SETTINGS.BASE).then(resp => {
            commit('SET_STATE', {param: 'appSettings', value: resp.data})
            commit('SET_LOAD', {param: 'loadSettings', flag: false})
          }).catch(err => {
            commit('SET_LOAD', {param: 'loadSettings', flag: false})
            console.log('err getSettings', err)
          })
        }
      },
      setParamFilters({commit, state}, paramFilters) {
        paramFilters = paramFilters || {}
        commit('SET_STATE', {param: 'paramFilters', value: paramFilters })
      },
      setParamFiltersByName({commit, state}, params) {
        let paramFilters = Object.assign({}, state.paramFilters)
                console.log(params)

        for (let k in params) {
          if (paramFilters.hasOwnProperty(k)) {
            paramFilters[k] = params[k]
          }
        }
        commit('SET_STATE', {param: 'paramFilters', value: paramFilters })
      },
      setSimpleState({commit, state}, {stateName, stateValue}) {
        commit('SET_STATE', {param: stateName, value: stateValue })
      },
      setPageDataParams({commit, state}, pageData) {
        let statePageData = Object.assign({}, state.pageDataParams)
        for (let k in statePageData) {
          if (pageData.hasOwnProperty(k)) {
            statePageData[k] = pageData[k]
          }
        }
        commit('SET_STATE', {param: 'pageDataParams', value: statePageData })
      },
      makeLiked({commit, state}, itemId) {
        let cookieContext = Object.assign({}, state.cookieContext)
        cookieContext.liked = Array.isArray(cookieContext.liked) ? cookieContext.liked : []
        let index = cookieContext.liked.findIndex( x => parseInt(x) === itemId )
        console.log('cookieContext, index', cookieContext.liked, index, itemId)
        index === -1 ? cookieContext.liked.push(itemId) : cookieContext.liked.splice(index, 1)
        commit('SET_STATE', {param: 'cookieContext', value: cookieContext })
      },
      getSiteReviews({state, commit}, params) {
        params = params || {}
        if (!state.loadReviews) {
          commit('SET_LOAD', {param: 'loadReviews', flag: true})
          return axios.get(apiUrls.REVIEW.SITE).then(resp => {
            commit('SET_STATE', {param: 'reviews', value: resp.data.results})
          }).catch(err => {
            console.log('====err ', err)
          }).finally( () => {
            commit('SET_LOAD', {param: 'loadReviews', flag: false})
          }) 
        }
      },
      addMsgs({state, commit}, snackMsgs) {
      
        commit('SET_LOAD', {param: 'loadSnackMsgs', flag: true})
        commit('SET_STATE', {param: 'snackMsgs', value: snackMsgs})
        commit('SET_LOAD', {param: 'loadSnackMsgs', flag: false})
      },
    },
    mutations: {
      SET_LOAD(state, {param, flag}){
        state[param] = flag
      },
      SET_CONTEXT(state, {k, v}){
        state.cookieContext[k] = v
      },
      SET_STATE(state, {param, value}){
        state[param] = value
      },
      PUSH_TO_STATE(state, {param, value}){
        state[param].push(value)
      },
      UPDATE_ITEM(state, {param, prop, propValue}){
        let i = state[param].findIndex( item => item[prop] === propValue[prop])
        if ( ~i ) {
          state[param][i] = propValue 
          state[param] = state[param]
        }
      },
      REMOVE_ITEM(state, {param, prop, propValue}){
        let i = state[param].findIndex( item => item[prop] === propValue)
        if ( ~i ) {
          state[param].splice(i, 1)
        }
      },
      SET_STATE_JSON(state, {paramName, dataParams, value}){
        state[paramName][JSON.stringify(dataParams)] = values
      },
      SET_STATE_BY_KEY(state, {paramName, k, values}){
        state[paramName][k] = values
      },
      CLEAN_USER(state){
        state.currentUser = {isAuth: false}
        state.cookieContext.usertoken = null
        delete state.cookieContext.usertoken
      },
    },

    getters: {
      getMenu: state => slug => state.menues.find(
          menu => menu.slug === slug
        ),
      getSettings: state => slug => (state.appSettings || {})[slug],
      getEntities: state =>  state.entityData.results || [],
      getEntitiesCount: state =>  state.entityData.count || 0,
      getTotalPages: state =>  state.entityData.total_pages || 0,
      getLiked: state =>  state.cookieContext.liked.map( x => parseInt(x)),
    }
  })
}