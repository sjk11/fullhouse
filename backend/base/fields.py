from django import forms
from django.db import models
from django.core.validators import RegexValidator

from django.utils.encoding import force_text
from django.utils.html import strip_tags
from django.utils.datastructures import MultiValueDict

from django.contrib.postgres.fields import ArrayField

from django_select2 import forms as s2forms


class SafeHtmlCharField(models.CharField):
    """
        При сохранении удаляет все HTML теги из текста, если в поле указан параметр strip=True
    """

    def __init__(self, strip=True, *args, **kwargs):
        self.strip = strip
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        value = super().to_python(value)
        if self.strip:
            value = strip_tags(value)
        return force_text(value)


class SafeHtmlTextField(models.TextField):
    """
        При сохранении удаляет все HTML теги из текста, если в поле указан параметр strip=True
    """

    def __init__(self, strip=True,
                 *args, **kwargs):
        self.strip = strip
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        value = super().to_python(value)
        if self.strip:
            value = strip_tags(value)
        return force_text(value)


class ArrayFieldSelectMultiple(forms.SelectMultiple):
    """This is a Form Widget for use with a Postgres ArrayField. It implements
    a multi-select interface that can be given a set of `choices`.
    You can provide a `delimiter` keyword argument to specify the delimeter used.
    """

    def __init__(self, *args, **kwargs):
        # Accept a `delimiter` argument, and grab it (defaulting to a comma)
        self.delimiter = kwargs.pop('delimiter', ',')
        super().__init__(*args, **kwargs)

    def value_from_datadict(self, data, files, name):
        if isinstance(data, MultiValueDict):
            # Normally, we'd want a list here, which is what we get from the
            # SelectMultiple superclass, but the SimpleArrayField expects to
            # get a delimited string, so we're doing a little extra work.
            return self.delimiter.join(data.getlist(name))

        return data.get(name)

    def get_context(self, name, value, attrs):
        value = None if value is None else value.split(self.delimiter)
        return super().get_context(name, value, attrs)


class CustomSelect2MultipleWidget(ArrayFieldSelectMultiple, s2forms.Select2MultipleWidget):
    delimiter = ','

    def __init__(self, *args, **kwargs):
        # Accept a `delimiter` argument, and grab it (defaulting to a comma)
        self.delimiter = kwargs.pop('delimiter', self.delimiter)
        super().__init__(*args, **kwargs)

    def value_from_datadict(self, data, files, name):
        # print('data, files, name', data, files, name, )
        if isinstance(data, MultiValueDict):
            # Normally, we'd want a list here, which is what we get from the
            # SelectMultiple superclass, but the SimpleArrayField expects to
            # get a delimited string, so we're doing a little extra work.
            return self.delimiter.join(data.getlist(name))

        return data.get(name)


phone_regex = RegexValidator(regex=r'^\+7[-\s]{1}?\d{3}?[-\s]{1}?\d{3}?[-\s]{1}?\d{2,18}$',
                             message="Номер телефона должен иметь формат: '+7-XXX-XXX-X[2-20]'. Не более 26 символов.")
