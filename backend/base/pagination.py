from django.conf import settings

from rest_framework import pagination
from rest_framework.response import Response


class Paginator(pagination.PageNumberPagination):
    page_size_query_param = settings.REST_FRAMEWORK.get('PAGINATE_BY_PARAM') if hasattr(settings, 'REST_FRAMEWORK') else None
    __qs = None

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'total_pages': self.page.paginator.num_pages,
            'page_size': self.get_page_size(self.request),
            'results': data,
        })

    def paginate_queryset(self, queryset, request, view=None):
        self.__qs = queryset
        return super().paginate_queryset(queryset, request, view)
