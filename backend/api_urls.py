from django.urls import path, include
from backend.base.views import SettingsViews

extra_patterns = [
    path('settings/', SettingsViews.as_view(), name='settings'),
    path('users/', include('backend.apps.users.api_urls', namespace='users')),
    path('content/', include('backend.apps.content.api_urls', namespace='content')),
    path('claim/', include('backend.apps.claim.api_urls', namespace='claim')),
    path('catalog/', include('backend.apps.catalog.api_urls', namespace='catalog')),
    path('review/', include('backend.apps.review.api_urls', namespace='review')),
]
