# Generated by Django 2.0 on 2018-12-23 16:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0002_navigation_navigationitem'),
    ]

    operations = [
        migrations.AlterField(
            model_name='navigationitem',
            name='menu',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='content.Navigation', verbose_name='меню'),
        ),
    ]
