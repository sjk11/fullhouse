
from django.contrib import admin

from backend.apps.content import models as content_models
from backend.apps.content import forms as content_forms


class BaseContentAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'is_active']
    search_fields = ['title']
    list_filter = ['is_active']


@admin.register(content_models.Page)
class PageAdmin(BaseContentAdmin):
    list_display = BaseContentAdmin.list_display + []
    search_fields = BaseContentAdmin.list_display + ['content']
    list_filter = BaseContentAdmin.list_display + ['is_active']


class NavigationItemInline(admin.TabularInline):
    model = content_models.NavigationItem
    extra = 1
    form = content_forms.NavigationItemAdminForm


@admin.register(content_models.Navigation)
class NavigationAdmin(BaseContentAdmin):
    inlines = [NavigationItemInline]
