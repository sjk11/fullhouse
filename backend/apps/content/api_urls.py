from django.urls import path
from rest_framework.routers import SimpleRouter

from backend.apps.content import views

router = SimpleRouter()
# router.register("report", views.ReportViewSet, base_name='report')  # user report
router.register("page", views.PageViewSet, base_name='page')  # moder report


app_name = 'content'
urlpatterns = [
    path('menues/', views.NavigationListView.as_view(), name='menu_list'),
] + router.urls
