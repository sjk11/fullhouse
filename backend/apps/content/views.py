from rest_framework import generics
from rest_framework import viewsets

from backend.apps.content import serializers as content_serializers


class NavigationListView(generics.ListAPIView):
    """ """

    def get_serializer_class(self):
        return content_serializers.NavigationSerializer

    def get_queryset(self):
        return self.get_serializer_class().Meta.model.objects.get_navaligation()


class PageViewSet(viewsets.ReadOnlyModelViewSet):

    lookup_field = 'slug'
    lookup_url_kwarg = 'slug'

    def get_serializer_class(self):
        return content_serializers.PageSerializer

    def get_queryset(self):
        return self.get_serializer_class().Meta.model.objects.actives()
