from rest_framework import serializers

from backend.apps.content import models as content_models


class NavigationItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = content_models.NavigationItem
        fields = '__all__'


class NavigationSerializer(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()

    class Meta:
        model = content_models.Navigation
        fields = '__all__'

    def get_items(self, obj):
        return NavigationItemSerializer(
            obj.items,
            many=True,
            context=self.context
        ).data


class PageSerializer(serializers.ModelSerializer):

    class Meta:
        model = content_models.Page
        fields = '__all__'
