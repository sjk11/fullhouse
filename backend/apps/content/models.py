from django.db import models

from ckeditor.fields import RichTextField

from backend.base.utils import unique_slugify


class ActiveQuerySet(models.QuerySet):

    def actives(self):
        return self.filter(is_active=True)


class NavigationQuerySet(ActiveQuerySet):

    def get_navaligation(self):
        return self.actives().prefetch_related(
            models.Prefetch(
                'items',
                queryset=NavigationItem.objects.actives()
            )
        )


class BaseContentModel(models.Model):
    title = models.CharField(verbose_name='Название', max_length=64, unique=True)
    slug = models.SlugField(verbose_name='Динамическая часть url', max_length=64, unique=True, blank=True)
    is_active = models.BooleanField(default=True, verbose_name='Отображено на сайте', db_index=True)
    objects = ActiveQuerySet.as_manager()

    class Meta:
        abstract = True

    def __str__(self):
        return f'{self._meta.verbose_name.title()} - {self.title}'

    def set_slug(self):
        self.slug = unique_slugify(self, self.title)

    def get_front_url(self):
        return f'/{self.__class__.__name__.lower()}/{self.slug}/'


class Navigation(BaseContentModel):
    TOP_MENU, MIDDLE_MENU, FOOTER_MENU = 'TOP_MENU', 'MIDDLE_MENU', 'FOOTER_MENU'
    posion_choice = (
        (TOP_MENU, 'сверху'),
        (MIDDLE_MENU, 'по-центру'),
        (FOOTER_MENU, 'в футере'),
    )
    slug = models.SlugField(verbose_name='расположение', choices=posion_choice, max_length=16, unique=True)

    objects = NavigationQuerySet.as_manager()

    class Meta:
        verbose_name = 'Навигация'
        verbose_name_plural = 'Навигация'

    def active_items(self):
        return self.items.actives()


class NavigationItem(BaseContentModel):
    menu = models.ForeignKey(Navigation, verbose_name='меню', on_delete=models.CASCADE, related_name='items')
    slug = models.CharField(verbose_name='url', max_length=256)
    is_special = models.BooleanField(default=False, verbose_name='Подсвечивать другим цветом')

    class Meta:
        verbose_name = 'Пункт меню'
        verbose_name_plural = 'Пункты меню'


class Page(BaseContentModel):
    content = RichTextField(verbose_name="Содержание страницы", blank=False, default='')

    class Meta:
        verbose_name = 'Cтатическая страница'
        verbose_name_plural = 'Cтатические страница'

    def save(self, *args, **kwargs):
        self.set_slug()
        return super().save(*args, **kwargs)
