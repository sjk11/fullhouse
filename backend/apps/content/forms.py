from django import forms
from backend.apps.content import models as content_models


class NavigationItemAdminForm(forms.ModelForm):
    slug = forms.CharField(widget=forms.TextInput)

    class Meta:
        model = content_models.NavigationItem
        fields = '__all__'
