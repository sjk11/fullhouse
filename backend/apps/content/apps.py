from django.apps import AppConfig


class ContentConfig(AppConfig):
    name = 'backend.apps.content'

    verbose_name = 'Контент'
    verbose_name_plural = 'Контент'
