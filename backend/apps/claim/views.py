from rest_framework import generics

from backend.apps.claim import serializers as claim_serializers


class CreateSelectClaimView(generics.CreateAPIView):
    """
        post:
        ** Создание новой заявки на подбор

    """
    authentication_classes = []
    permission_classes = []

    def get_serializer_class(self):
        return claim_serializers.SelectionClaimSerializer

    def get_queryset(self):
        return self.get_serializer_class().Meta.model.objects.all()


class CreateOwnerClaimView(generics.CreateAPIView):
    """
        post:
        ** Создание новой заявки на подбор

    """
    authentication_classes = []
    permission_classes = []

    def get_serializer_class(self):
        return claim_serializers.OwnerClaimSerializer

    def get_queryset(self):
        return self.get_serializer_class().Meta.model.objects.all()
