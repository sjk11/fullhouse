from django.apps import AppConfig


class ClaimConfig(AppConfig):
    name = 'backend.apps.claim'

    verbose_name = 'Заявка'
    verbose_name_plural = 'Заявки'
