from rest_framework import serializers

from drf_extra_fields.fields import IntegerRangeField, DateRangeField

from backend.apps.claim import models as claim_models


class SelectionClaimSerializer(serializers.ModelSerializer):
    stay_dates = DateRangeField()
    guests_count = IntegerRangeField()
    prices = IntegerRangeField()

    class Meta:
        model = claim_models.SelectionClaim
        exclude = ['created_dt', 'status']

    def validate_stay_dates(self, data):
        return data


class OwnerClaimSerializer(serializers.ModelSerializer):

    class Meta:
        model = claim_models.OwnerClaim
        exclude = ['created_dt', 'status']
