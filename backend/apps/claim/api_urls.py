from django.urls import path

from backend.apps.claim import views

app_name = 'claim'

urlpatterns = [
    path('add/selection/', views.CreateSelectClaimView.as_view(), name='create_selection_claim'),
    path('add/owner/', views.CreateOwnerClaimView.as_view(), name='create_owner_claim'),
]
