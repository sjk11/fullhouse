from django.db import models
from django.contrib.postgres.fields import IntegerRangeField, DateRangeField


from backend.base.fields import phone_regex, SafeHtmlCharField


class BaseQuerySet(models.QuerySet):
    pass


class BaseClaimModel(models.Model):
    NEW, GOOD, BAD = 0, 10, 20
    status_choice = (
        (NEW, 'новая'),
        (GOOD, 'принята'),
        (BAD, 'отклонена'),
    )
    name = SafeHtmlCharField(
        verbose_name='Имя', max_length=32, blank=True, default=''
    )
    phone = SafeHtmlCharField(
        verbose_name='Телефон', max_length=26,
        validators=[phone_regex], blank=False
    )
    email = models.EmailField(
        verbose_name='Email', max_length=255, blank=True, default=''
    )
    created_dt = models.DateTimeField(verbose_name='Дата написания', auto_now_add=True, db_index=True)

    status = models.PositiveSmallIntegerField(choices=status_choice, default=NEW)

    objects = BaseQuerySet.as_manager()

    class Meta:
        abstract = True


class OwnerClaim(BaseClaimModel):
    class Meta:
        verbose_name = 'Заявка собственника'
        verbose_name_plural = 'Заявки собственника'


class SelectionClaim(BaseClaimModel):
    stay_dates = DateRangeField(verbose_name='Даты пребывания')
    guests_count = IntegerRangeField(verbose_name='Кол-во гостей')
    prices = IntegerRangeField(verbose_name='Стоимость')

    to_entity = models.ForeignKey('catalog.Entity',
                                  blank=True,
                                  null=True,
                                  default=None,
                                  on_delete=models.SET_NULL,
                                  related_name='claims', verbose_name='Объект')

    class Meta:
        verbose_name = 'Заявка на подбор'
        verbose_name_plural = 'Заявки на подбор'
