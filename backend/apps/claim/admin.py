from django.contrib import admin
from backend.apps.claim import models as claim_models
from ajax_select import make_ajax_form


class BaseClaimAdmin(admin.ModelAdmin):
    list_display = ['phone', 'name', 'email', 'created_dt']
    search_fields = ['phone', 'name', 'email', ]
    list_filter = ['created_dt', 'status']


@admin.register(claim_models.OwnerClaim)
class OwnerClaimAdmin(BaseClaimAdmin):
    pass


@admin.register(claim_models.SelectionClaim)
class SelectionClaimAdmin(BaseClaimAdmin):
    list_display = BaseClaimAdmin.list_display + ['stay_dates', 'guests_count', 'prices']
    search_fields = BaseClaimAdmin.search_fields + ['stay_dates']
    list_filter = BaseClaimAdmin.search_fields + ['stay_dates']
    form = make_ajax_form(claim_models.SelectionClaim, {
        'to_entity': 'entities'      # ForeignKeyField
    })
