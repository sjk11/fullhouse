from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView

from backend.apps.catalog import serializers as catalog_serializers
from backend.apps.catalog.extra import EnitityFilter


class FrontFilterApiView(APIView):
    """
    get:
    ** список фильтров
    """

    allowed_methods = ['GET', 'OPTIONS']

    def get(self, request, *args, **kwargs):
        data, context = {}, {}
        data['EntityType'] = catalog_serializers.EntityTypeSerializer(
            catalog_serializers.EntityTypeSerializer.Meta.model.objects.all(),
            many=True, context=context
        ).data
        data['Entity'] = catalog_serializers.EntitySerializer.Meta.model.objects.catalog_aggreagates()
        data['extraFilters'] = {}
        class_attr = catalog_serializers.AttributeFilterSerializer.Meta.model
        for AttrName, label in class_attr.ATTR_CHOICE:
            data['extraFilters'][AttrName] = catalog_serializers.AttributeFilterSerializer(
                class_attr.objects.get_extra_filters(AttrName),
                many=True,
                context=context).data
        return Response(data=data, status=status.HTTP_200_OK)


class EntityListView(generics.ListAPIView):

    filterset_class = EnitityFilter

    search_fields = ('=id', 'title', 'share')
    ordering_fields = ('price_workday', 'price_holyday', 'bulk_man', 'textattr_attrs__value')

    def get_serializer_class(self):
        return catalog_serializers.EntitySerializer

    def get_queryset(self):
        return self.get_serializer_class().Meta.model.objects.catalogall()


class EntityDetailView(generics.RetrieveAPIView):

    filterset_class = EnitityFilter

    lookup_field = 'slug'

    def get_serializer_class(self):
        return catalog_serializers.DetailEntitySerializer

    def get_queryset(self):
        return self.get_serializer_class().Meta.model.objects.catalogall()


class EntityAutocompleteView(APIView):

    allowed_methods = ['GET', 'OPTIONS']

    serializer_class = catalog_serializers.EntityValuesSerializer

    def get(self, request, **kwargs):
        return Response(
            status=status.HTTP_200_OK,
            data=self.serializer_class(
                self.get_objects(self.request.query_params.get('q')),
                many=True
            ).data
        )

    def get_objects(self, q):
        objs = self.serializer_class.Meta.model.objects.actives()
        if q:
            objs = objs.filter(title__icontains=q)
        return objs.values('id', 'title')[:50]
