from rest_framework import serializers

from drf_extra_fields.fields import IntegerRangeField, DateRangeField

from backend.apps.catalog import models as catalog_models

from backend.apps.review.serializers import ReviewSerializer


class EntityTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = catalog_models.EntityType
        fields = '__all__'


class AttributeFilterSerializer(serializers.ModelSerializer):
    img_url = serializers.SerializerMethodField()
    list_values = serializers.SerializerMethodField()

    class Meta:
        model = catalog_models.Attribute
        fields = '__all__'

    def get_img_url(self, obj):
        if obj.image:
            return obj.image.url

    def get_list_values(self, obj):
        return obj.get_list_values()


class AttributeEntitySerializer(AttributeFilterSerializer):
    rel_value = serializers.SerializerMethodField()

    class Meta:
        model = catalog_models.Attribute
        fields = '__all__'

    def get_rel_value(self, obj):
        return obj.get_rel_value()


class EntityValuesSerializer(serializers.ModelSerializer):

    class Meta:
        model = catalog_models.Entity
        fields = ('id', 'title')


class EntitySerializer(serializers.ModelSerializer):
    img_url = serializers.SerializerMethodField()
    imgs = serializers.SerializerMethodField()
    is_share = serializers.SerializerMethodField()
    show_attributes = serializers.SerializerMethodField()
    latlng = serializers.SerializerMethodField()
    static_attrs = serializers.SerializerMethodField()

    class Meta:
        model = catalog_models.Entity
        fields = '__all__'

    def get_img_url(self, obj):
        if obj.image:
            return obj.image.url
        return '/backstatic/img/noimage.png'

    def get_imgs(self, obj):
        return obj.images_url()

    def get_is_share(self, obj):
        return bool(obj.is_share())

    def get_show_attributes(self, obj):
        show_attributes = {}
        for show_in, attrs in obj.get_show_attributes().items():
            show_attributes[show_in] = AttributeEntitySerializer(attrs, many=True, context=self.context).data
        return show_attributes

    def get_latlng(self, obj):
        coord = {}
        coord['lat'], coord['lng'] = obj.coord.coords
        return coord

    def get_static_attrs(self, obj):
        return {
            'bulk_man': {
                'img_url': '/backstatic/img/man-icon-black-hi.svg',
                'title': 'Вместимость',
                'value': obj.bulk_man,
            },
        }


class DetailEntitySerializer(EntitySerializer):
    seodata = serializers.SerializerMethodField()
    reviews = serializers.SerializerMethodField()
    related_entitys = serializers.SerializerMethodField()
    bool_attrs = serializers.SerializerMethodField()

    def get_seodata(self, obj):
        return obj.get_seodata() or {}

    def get_reviews(self, obj):
        return ReviewSerializer(obj.reviews.actives(), many=True, context=self.context).data

    def get_related_entitys(self, obj):
        return EntitySerializer(obj.get_related_entitys(), many=True, context=self.context).data

    def get_bool_attrs(self, obj):
        return BoolAttrSerializer(obj.boolattr_attrs.all(), many=True, context=self.context).data


class BaseAttrSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'


class IntAttrSerializer(BaseAttrSerializer):

    class Meta(BaseAttrSerializer.Meta):
        model = catalog_models.IntAttr


class BoolAttrSerializer(BaseAttrSerializer):

    class Meta(BaseAttrSerializer.Meta):
        model = catalog_models.BoolAttr


class TextAttrSerializer(BaseAttrSerializer):

    class Meta(BaseAttrSerializer.Meta):
        model = catalog_models.TextAttr


class ManyChoiceAttrSerializer(BaseAttrSerializer):

    class Meta(BaseAttrSerializer.Meta):
        model = catalog_models.ManyChoiceAttr
