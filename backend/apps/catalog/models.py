from django.db import models
from django.apps import apps

from django.contrib.gis.db.models import PointField
from django.contrib.gis.geos import Point
from django.db.models.functions import Length

from django.contrib.postgres.fields import ArrayField

from fontawesome.fields import IconField

from backend.base.utils import unique_slugify, generate_upload_name

from backend.apps.seo.mixins import SeoDataMixin
from backend.apps.review.mixins import ReviewMixin


class ActiveQuerySet(models.QuerySet):

    def actives(self, **f):
        f['is_active'] = True
        return self.filter(**f)


class EntityQuerySet(ActiveQuerySet):
    def catalogall(self):
        prf = [f'{x[0].lower()}_attrs' for x in Attribute.ATTR_CHOICE]
        return self.annotate(
            len_share=Length('share')
        ).actives().prefetch_related(*prf).select_related(
            'entity_type'
        ).order_by('-sort', '-id')

    def catalog_aggreagates(self):
        return self.actives().aggregate(
            max_price_workday=models.Max('price_workday'),
            min_price_workday=models.Min('price_workday'),
            max_price_holyday=models.Max('price_holyday'),
            min_price_holyday=models.Min('price_holyday'),
            max_bulk_man=models.Max('bulk_man'),
            min_bulk_man=models.Min('bulk_man'),
        )


class AttributeQuerySet(ActiveQuerySet):

    def with_pref(self):
        prf = [f'{x[0].lower()}_vals' for x in Attribute.ATTR_CHOICE]
        return self.actives().prefetch_related(*prf)

    def with_values(self, entity):
        annotates = {}
        for x in Attribute.ATTR_CHOICE:
            annotates[f'{x[0]}__value'] = models.Subquery(apps.get_model('catalog', x[0]).objects.filter(
                attr=models.OuterRef('pk'),
                entity=entity
            ).values('value')[:1])
        return self.annotate(**annotates)

    def get_extra_filters(self, attr_type):
        return self.actives(is_extra_filters=True, attr_type=attr_type)


class BaseCatalogModel(models.Model):
    title = models.CharField(verbose_name='Название', max_length=128, unique=True)
    slug = models.SlugField(verbose_name='Динамическая часть url', max_length=128, unique=True, blank=True)
    sort = models.PositiveSmallIntegerField(db_index=True, default=100, verbose_name='Сорт.')

    class Meta:
        abstract = True
        ordering = ['-sort', '-id']

    def __str__(self):
        return f'{self._meta.verbose_name.title()} - {self.title}'

    def set_slug(self):
        self.slug = unique_slugify(self, self.title)

    def save(self, *args, **kwargs):
        self.set_slug()
        return super().save(*args, **kwargs)

    def get_front_url(self):
        return f'/{self.__class__.__name__.lower()}/{self.slug}/'


class EntityType(BaseCatalogModel, SeoDataMixin):

    class Meta:
        verbose_name = 'Тип объекта'
        verbose_name_plural = 'Типы объектов'


class Entity(BaseCatalogModel, SeoDataMixin, ReviewMixin):
    entity_type = models.ForeignKey(EntityType, blank=False, null=True, default=None, related_name='entities', on_delete=models.SET_NULL)
    price_workday = models.PositiveIntegerField('Стоимость в будни')
    price_holyday = models.PositiveIntegerField('Стоимость в выходные')
    is_active = models.BooleanField(default=True, verbose_name='Активно', db_index=True)
    share = models.TextField(default='', verbose_name='Акция', blank=True)
    coord = PointField(verbose_name='координаты', srid=4326, blank=False)
    bulk_man = models.IntegerField(db_index=True, verbose_name='Вместимость', blank=False, default=0)

    objects = EntityQuerySet.as_manager()
    _main_image = False

    class Meta:
        verbose_name = 'Объект недвижимости'
        verbose_name_plural = 'Объекты недвижимости'

    def main_image(self):
        if self._main_image is False:
            self._main_image = self.images.filter(is_main=True).first()
        return self._main_image

    def images_url(self):
        return [x.image.url for x in self.images.order_by('-is_main')]

    def is_share(self):
        if hasattr(self, 'len_share') is False:
            setattr(self, 'len_share', len(self.share))
        return bool(self.len_share)

    @property
    def image(self):
        if self.main_image() is not None:
            return self._main_image.image

    def get_attributes(self):
        return {
            x[0]: getattr(self, f'{x[0].lower()}_attr') for x in Attribute.ATTR_CHOICE
        }

    def get_show_attributes(self):
        values = {x[0]: [] for x in Attribute.SHOW_CHOICE}
        for attr in Attribute.objects.with_values(self):
            for show_in in attr.show_in:
                values[show_in].append(attr)
        return values

    def get_type_attributes(self):
        # attr_types = {x[0]: [] for x in Attribute.ATTR_CHOICE}
        # for attr_type, label in Attribute.ATTR_CHOICE:
        #     for attr in Attribute.objects.filter(attr_type=attr_type).with_values(self):
        #         for show_in in attr.show_in:
        #             values[show_in].append(attr)
        return []

    def get_seodata(self):
        seodata = self.seodata.first()
        if self.seodata.first():
            return seodata.get_seo_data()

    def get_related_entitys(self):
        if self.entity_type is not None:
            return self.entity_type.entities.order_by('?')[:4]


class Attribute(BaseCatalogModel):
    is_active = models.BooleanField(default=True, verbose_name='Активно')
    SHOW_LIST, SHOW_BLOCK, SHOW_MAP, SHOW_DETAIL = 'LIST', 'BLOCK', 'MAP', 'DETAIL'
    SHOW_CHOICE = [
        (SHOW_LIST, 'В списке'),
        (SHOW_BLOCK, 'В плитке'),
        (SHOW_MAP, 'На карте'),
        (SHOW_DETAIL, 'На Детальной страницу'),
    ]
    show_in = ArrayField(
        models.CharField(choices=SHOW_CHOICE, max_length=6),
        verbose_name='Отоброжение',
        blank=True, default=[]
    )
    IntAttr, BoolAttr, TextAttr, ManyChoiceAttr = 'IntAttr', 'BoolAttr', 'TextAttr', 'ManyChoiceAttr'
    ATTR_CHOICE = (
        (IntAttr, 'Число'),
        (BoolAttr, 'Да/Нет'),
        (TextAttr, 'Текст'),
        (ManyChoiceAttr, 'Выбор значений'),
    )

    attr_type = models.CharField(max_length=16, choices=ATTR_CHOICE, verbose_name='Тип атрибута')
    measure = models.CharField(max_length=24, blank=True, default='', verbose_name='Ед.изм')
    icon = IconField(default='', blank=True, verbose_name='Иконка')
    image = models.FileField(upload_to=generate_upload_name, verbose_name='Изобр.(замена иконки)', blank=True, default='')
    is_extra_filters = models.BooleanField(default=False, verbose_name='Отображать как доп.фильтр')

    objects = AttributeQuerySet.as_manager()

    class Meta:
        verbose_name = 'Атрибут/фильтр'
        verbose_name_plural = 'Атрибуты/фильтры'

    def get_attr_model(self):
        return apps.get_model('catalog', self.attr_type)

    def get_attr_related(self):
        if hasattr(self, f'{self.attr_type.lower()}_vals'):
            return getattr(self, f'{self.attr_type.lower()}_vals')

    def get_rel_value(self):
        if hasattr(self, f'{self.attr_type}__value'):
            return getattr(self, f'{self.attr_type}__value')

    def get_list_values(self):
        if self.get_attr_related() is not None:
            return self.get_attr_related().values('value').annotate(c=models.Count('value')).values_list('value', flat=True)


class AttributeChoice(models.Model):
    attr = models.ForeignKey(Attribute, verbose_name='Атрибут', related_name="attr_choice", on_delete=models.CASCADE)
    icon = IconField(default='', blank=True, verbose_name='Иконка')
    choice_value = models.CharField(max_length=128, verbose_name='Значение')

    class Meta:
        verbose_name = 'Значение для выбора'
        verbose_name_plural = 'Значение для выбора'


class BaseAttribute(models.Model):
    attr = models.ForeignKey(Attribute, verbose_name='Атрибут', related_name="%(class)s_vals", on_delete=models.CASCADE)
    entity = models.ForeignKey(Entity, verbose_name='Объект', related_name="%(class)s_attrs", on_delete=models.CASCADE)

    class Meta:
        abstract = True
        unique_together = (('attr', 'entity'), )

    def get_type_name(self):
        return self.__class__.__name__


class IntAttr(BaseAttribute):
    value = models.IntegerField(db_index=True, verbose_name='Значения')

    class Meta:
        verbose_name = 'Атрибут число'
        verbose_name_plural = 'Атрибут числа'
        unique_together = (('attr', 'entity'), )


class BoolAttr(BaseAttribute):
    value = models.BooleanField(db_index=True, default=False, verbose_name='Значения')

    class Meta:
        verbose_name = 'Атрибут "Да/нет"'
        verbose_name_plural = 'Атрибут "Да/нет"'
        unique_together = (('attr', 'entity'), )


class TextAttr(BaseAttribute):
    value = models.TextField(blank=True, default='', verbose_name='Значения')

    class Meta:
        verbose_name = 'Атрибут Текст'
        verbose_name_plural = 'Атрибут Текст'
        unique_together = (('attr', 'entity'), )


class ManyChoiceAttr(BaseAttribute):
    value = ArrayField(
        models.CharField(max_length=128),
        verbose_name='Значения',
    )

    class Meta:
        verbose_name = 'Атрибут Выбор значений'
        verbose_name_plural = 'Атрибут Выбор значений'
        unique_together = (('attr', 'entity'), )


class ImageEntity(models.Model):
    sort = models.PositiveSmallIntegerField(db_index=True, default=100, verbose_name='Сорт.')
    image = models.ImageField(upload_to=generate_upload_name, verbose_name='Фото объекта')
    entity = models.ForeignKey(Entity, verbose_name='Объект', related_name="images", on_delete=models.CASCADE)
    is_main = models.BooleanField(default=False, verbose_name='Это основное фото')

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фото'
        ordering = ['-sort', '-id']

    def set_main(self):
        if self.is_main:
            self.entity.images.filter(is_main=self.is_main).update(is_main=False)

    def save(self, *args, **kwargs):
        self.set_main()
        super().save(*args, **kwargs)
