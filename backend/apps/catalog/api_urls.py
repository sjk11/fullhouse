from django.urls import path

from backend.apps.catalog import views


app_name = 'catalog'

urlpatterns = [
    path('filters/', views.FrontFilterApiView.as_view(), name='front_filters'),
    path('entities/', views.EntityListView.as_view(), name='entity_list'),
    path('entity_autocomplete/', views.EntityAutocompleteView.as_view(), name='entity_autocomplete'),
    path('entity/<slug:slug>/', views.EntityDetailView.as_view(), name='entity_detail'),
]
