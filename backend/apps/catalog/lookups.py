from django.db.models import Q

from ajax_select import register, LookupChannel

from .models import Entity


@register('entities')
class EntityLookup(LookupChannel):
    model = Entity

    def get_query(self, q, request):
        return self.model.objects.filter(Q(title__icontains=q) | Q(id__exact=q))[:50]

    def format_item_display(self, obj):
        return f'<a href="/admin/catalog/entity/{obj.id}/change/" class="tagcity" target="_blank">{obj.title} #{obj.pk}</a>'
