from django.contrib import admin

from constance import config

from backend.apps.review.admin import ReviewInlineAdmin
from backend.apps.seo.admin import SeoDataInlineAdmin

from backend.apps.catalog import models as catalog_models
from backend.apps.catalog import forms as catalog_forms


class BaseAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'sort')
    search_fields = ['title']
    list_editable = ['sort']

    class Media:
        js = ['js/admin.js']


@admin.register(catalog_models.EntityType)
class EntityTypeAdmin(BaseAdmin):
    pass


class ImageEntityInline(admin.TabularInline):
    model = catalog_models.ImageEntity
    extra = 1


class BaseAttrInline(admin.TabularInline):
    extra = 0

    def get_formset(self, request, obj=None, **kwargs):
        self.formset = getattr(catalog_forms, f'{self.model.__name__}ModelInlineFormSet')
        formset = super().get_formset(request, obj, **kwargs)
        formset.request = request
        self.form = getattr(catalog_forms, f'{self.model.__name__}Form')
        return formset

    def get_extra(self, request, obj=None, **kwargs):
        extra = catalog_models.Attribute.objects.filter(
            attr_type=self.model.__name__
        ).count()
        if obj is not None:
            extra -= self.model.objects.filter(entity=obj).count()
        return extra

    # def has_add_permission(self, request):
    #     self.extra = self.attr_count = catalog_models.Attribute.objects.filter(
    #         attr_type=self.model.__name__
    #     ).count()
    #     if self.extra:
    #         self.extra -= 1
    #     print('has_add_permission', 'has_add_permission')

    #     return self.attr_count < self.extra


class IntAttrInline(BaseAttrInline):
    model = catalog_models.IntAttr


class BoolAttrInline(BaseAttrInline):
    model = catalog_models.BoolAttr


class TextAttrInline(BaseAttrInline):
    model = catalog_models.TextAttr


class ManyChoiceAttrInline(BaseAttrInline):
    model = catalog_models.ManyChoiceAttr


@admin.register(catalog_models.Entity)
class EntityAdmin(BaseAdmin):
    list_display = BaseAdmin.list_display + ('entity_type', 'price_workday', 'price_holyday', 'is_active', 'coord')
    search_fields = BaseAdmin.search_fields + ['entity_type__title']
    list_filter = ('is_active',)
    form = catalog_forms.EntityForm
    inlines = [
        ImageEntityInline,
        SeoDataInlineAdmin,
        ReviewInlineAdmin,
        IntAttrInline,
        BoolAttrInline,
        TextAttrInline,
        ManyChoiceAttrInline,
    ]

    def get_form(self, request, obj=None, **kwargs):
        model_form = super().get_form(request, obj, **kwargs)

        class ModelFormMetaClass(model_form):
            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                return model_form(*args, **kwargs)
        return ModelFormMetaClass


class AttributeChoiceInline(admin.StackedInline):
    model = catalog_models.AttributeChoice

    def get_extra(self, request, obj=None, **kwargs):
        if obj is not None:
            if obj.attr_type in (catalog_models.Attribute.ManyChoiceAttr,):
                return 1
        return 0


@admin.register(catalog_models.Attribute)
class AttributeAdmin(BaseAdmin):
    list_display = BaseAdmin.list_display + ('show_in',)
    list_filter = BaseAdmin.search_fields + ['is_active', 'show_in', 'attr_type']
    inlines = []
    request = None
    form = catalog_forms.AttributeForm

    def get_fieldsets(self, request, obj=None):

        if (obj is not None and obj.attr_type in (catalog_models.Attribute.ManyChoiceAttr,)):
            self.inlines = [AttributeChoiceInline]
        return super().get_fieldsets(request, obj)

    def get_form(self, request, obj=None, **kwargs):
        model_form = super().get_form(request, obj, **kwargs)

        class ModelFormMetaClass(model_form):
            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                return model_form(*args, **kwargs)
        return ModelFormMetaClass
