import json

from django import forms
from django.contrib.gis.geos import Point
from django.utils import six
# from django.contrib.gis.geos import Point
# from django.contrib.postgres.forms import SimpleArrayField

from mapwidgets.widgets import GooglePointFieldWidget


from backend.base.fields import ArrayFieldSelectMultiple, CustomSelect2MultipleWidget


from backend.apps.catalog import models as catalog_models


class MakeGooglePointFieldWidget(GooglePointFieldWidget):
    def render(self, name, value, attrs=None, renderer=None):
        if attrs is None:
            attrs = dict()

        field_value = {}

        if isinstance(value, Point):
            if value.srid and value.srid != self.google_map_srid:
                ogr = value.ogr
                ogr.transform(self.google_map_srid)
                value = ogr
            longitude, latitude = value.coords
            field_value["lat"] = longitude
            field_value["lng"] = latitude

        extra_attrs = {
            "options": self.map_options(),
            "field_value": json.dumps(field_value)
        }
        attrs.update(extra_attrs)
        self.as_super = super(GooglePointFieldWidget, self)
        if renderer is not None:
            return self.as_super.render(name, value, attrs, renderer)
        else:
            return self.as_super.render(name, value, attrs)


class EntityForm(forms.ModelForm):

    class Meta:
        model = catalog_models.Entity
        fields = "__all__"
        widgets = {'coord': MakeGooglePointFieldWidget()}

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)

    def clean(self):
        data = super().clean()
        # print('self.request.POST', self.request.POST.get('coord'))
        # print('self.request.POST', data['coord'])
        return data


class AttributeForm(forms.ModelForm):
    # show_in_view = ArrayFieldSelectMultiple(widget=Select2MultipleWidget, choices=catalog_models.Attribute.SHOW_CHOICE, label='Отоброжение')
    # show_in = SimpleArrayField(forms.ChoiceField(choices=catalog_models.Attribute.SHOW_CHOICE))

    class Meta:
        model = catalog_models.Attribute
        fields = "__all__"
        widgets = {
            'show_in': ArrayFieldSelectMultiple(choices=catalog_models.Attribute.SHOW_CHOICE),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        if 'initial' in kwargs:
            kwargs['initial']['show_in'] = kwargs['initial'].get('show_in') or []
        super().__init__(*args, **kwargs)


class AttrModelInlineFormSet(forms.models.BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        kwargs['initial'] = []
        instance = kwargs.get('instance')
        for attr in catalog_models.Attribute.objects.filter(attr_type=self.model.__name__):
            initial_data = attr.get_attr_related().filter(
                entity=instance).values(
                'id', 'value').first() or {}
            initial_data['attr'] = attr
            kwargs['initial'].append(initial_data)
        super().__init__(*args, **kwargs)

    def _construct_form(self, i, **kwargs):
        form = super()._construct_form(i, **kwargs)
        form.request = self.request
        return form


class IntAttrModelInlineFormSet(AttrModelInlineFormSet):
    pass


class BoolAttrModelInlineFormSet(AttrModelInlineFormSet):
    pass


class TextAttrModelInlineFormSet(AttrModelInlineFormSet):
    pass


class ManyChoiceAttrModelInlineFormSet(AttrModelInlineFormSet):
    pass


class AttrInlineForm(forms.ModelForm):
    # readonly_fields = ('attr', )

    class Meta:
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance:
            self.fields['attr'].choices = catalog_models.Attribute.objects.filter(
                attr_type__iexact=self.instance.__class__.__name__
            ).values_list('id', 'title')
        self.fields['value'].widget.attrs['class'] = f'{self.__class__.__name__.lower()}--value'


class IntAttrForm(AttrInlineForm):

    class Meta(AttrInlineForm.Meta):
        model = catalog_models.IntAttr


class BoolAttrForm(AttrInlineForm):

    class Meta(AttrInlineForm.Meta):
        model = catalog_models.BoolAttr


class TextAttrForm(AttrInlineForm):

    class Meta(AttrInlineForm.Meta):
        model = catalog_models.TextAttr


class ManyChoiceAttrForm(AttrInlineForm):

    class Meta(AttrInlineForm.Meta):
        model = catalog_models.ManyChoiceAttr
        # widgets = {
        #     'value': CustomSelect2MultipleWidget(
        #         choices=catalog_models.AttributeChoice.objects.values_list('id', 'choice_value')
        #     )
        # }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['value'].widget = CustomSelect2MultipleWidget(
            choices=catalog_models.AttributeChoice.objects.values_list('id', 'choice_value')
        )
