from django.utils import timezone
import django_filters
from backend.apps.catalog import models as catalog_models


class SlugInFilter(django_filters.BaseInFilter, django_filters.CharFilter):
    pass


class EnitityFilter(django_filters.FilterSet):
    len_share = django_filters.NumberFilter(method='get_len_share')
    price__gte = django_filters.NumberFilter(method='get_price__gte')
    price__lte = django_filters.NumberFilter(method='get_price__lte')
    stay_date__gte = django_filters.DateFilter(input_formats=['%d/%m/%Y'], method='get_stay_date__gte')
    stay_date__lte = django_filters.DateFilter(input_formats=['%d/%m/%Y'], method='get_stay_date__lte')
    # entitytype = SlugInFilter(method='get_entitytype')
    entitytype = django_filters.filters.ModelMultipleChoiceFilter(
        field_name='entity_type__slug',
        to_field_name='slug',
        queryset=catalog_models.EntityType.objects.all(),
        lookup_expr='in'
    )
    ids = django_filters.filters.ModelMultipleChoiceFilter(
        field_name='id',
        to_field_name='id',
        queryset=catalog_models.Entity.objects.actives(),
        lookup_expr='in'
    )

    class Meta:
        model = catalog_models.Entity
        fields = {
            'price_workday': ['lte', 'gte'],
            'price_holyday': ['lte', 'gte'],
            'bulk_man': ['lte', 'gte'],
            'id': ['exact'],
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cleaned_data = {}
        if self.is_valid():
            self.cleaned_data = self.form.clean()
        self.is_holydays = self.is_only_holyday(self.cleaned_data.get('stay_date__gte'), self.cleaned_data.get('stay_date__lte'))

    def get_price_filter(self, lookup_expr, value):
        filter_field = 'price_workday'
        if self.is_holydays:
            filter_field = 'price_holyday'
        return {f'{filter_field}__{lookup_expr}': value}

    def get_price__gte(self, queryset, name, value):
        return queryset.filter(**self.get_price_filter('gte', value))

    def get_price__lte(self, queryset, name, value):
        return queryset.filter(**self.get_price_filter('lte', value))

    def get_stay_date__gte(self, queryset, name, value):
        return queryset

    def get_stay_date__lte(self, queryset, name, value):
        return queryset

    def is_only_holyday(self, date_start, date_end):
        date_start = date_start or timezone.now().today()
        if date_start and date_end:
            return self.is_holyday(date_start) and self.is_holyday(date_end)
        return False

    def is_holyday(self, check_date):
        return check_date.weekday() > 4  # Digits 0-6 represent the consecutive weekdays, starting from Monday

    def get_entitytype(self, queryset, name, value):
        return queryset

    def get_len_share(self, queryset, name, value):
        if value:
            return queryset.filter(len_share__gt=0)
        return queryset
