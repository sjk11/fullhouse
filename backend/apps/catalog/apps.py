from django.apps import AppConfig


class CatalogConfig(AppConfig):
    name = 'backend.apps.catalog'

    verbose_name = 'Каталог'
    verbose_name_plural = 'Каталог'

    # def ready(self):
    #     from . import signals
