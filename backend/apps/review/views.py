from rest_framework import generics

from backend.apps.review import serializers as review_serializers
from backend.apps.catalog.models import Entity
from django.http import Http404


class CreateEntityReview(generics.CreateAPIView):
    authentication_classes = []
    permission_classes = []

    def get_serializer_class(self):
        return review_serializers.ReviewCreateSerializer

    def get_queryset(self):
        return review_serializers.ReviewCreateSerializer.Meta.model.objects.all()

    def get_model(self):
        return review_serializers.ReviewCreateSerializer.Meta.model

    def get_serializer_context(self):
        context = {}
        entity = Entity.objects.actives(id=self.kwargs['entity_id']).first()
        if entity is None:
            raise Http404
        context['obj'] = entity
        return context


class ListSiteCreateReview(generics.ListCreateAPIView):
    authentication_classes = []
    permission_classes = []

    def get_serializer_class(self):
        return review_serializers.SiteReviewSerializer

    def get_queryset(self):
        return self.get_serializer_class().Meta.model.objects.actives()
