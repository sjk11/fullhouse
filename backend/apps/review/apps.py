from django.apps import AppConfig


class ReviewConfig(AppConfig):
    name = 'backend.apps.review'

    verbose_name = 'Отзыв'
    verbose_name_plural = 'Отзывы'
