from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline

from backend.apps.review import models as review_models


class BaseReviewAdmin(admin.ModelAdmin):
    list_display = ['id', 'phone', 'name', 'created_dt', 'is_active']
    search_fields = ['phone', 'name', 'email']
    list_filter = ['is_active', 'created_dt']
    list_editable = ['is_active']


@admin.register(review_models.SiteReview)
class SiteReviewAdmin(BaseReviewAdmin):
    pass


@admin.register(review_models.Review)
class ReviewAdmin(BaseReviewAdmin):
    pass


class ReviewInlineAdmin(GenericStackedInline):
    model = review_models.Review
    extra = 0
