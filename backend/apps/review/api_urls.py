from django.urls import path

from backend.apps.review import views

app_name = 'review'

urlpatterns = [
    path('add/entity/<int:entity_id>/', views.CreateEntityReview.as_view(), name='create_entity_review'),
    path('site/', views.ListSiteCreateReview.as_view(), name='site_review')
]
