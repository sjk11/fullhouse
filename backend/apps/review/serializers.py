from rest_framework import serializers


from backend.apps.review import models as review_models


class ReviewSerializer(serializers.ModelSerializer):
    created = serializers.SerializerMethodField()

    class Meta:
        model = review_models.Review
        exclude = ('content_type', 'object_id', 'phone')

    def get_created(self, obj):
        if obj.created_dt:
            return obj.created_dt.strftime("%d.%m.%Y")


class ReviewCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = review_models.Review
        exclude = ('content_type', 'object_id', 'created_dt', 'is_active')

    def create(self, data):

        return self.context['obj'].reviews.create(**data)


class SiteReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = review_models.SiteReview
        exclude = ('created_dt', 'is_active')
