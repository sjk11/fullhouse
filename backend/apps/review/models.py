from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from django.db import models

from backend.base.fields import SafeHtmlTextField, SafeHtmlCharField, phone_regex


class BaseQuerySet(models.QuerySet):

    def actives(self, **filters):
        filters['is_active'] = True
        return self.filter(**filters)


class BaseReviewModel(models.Model):
    name = SafeHtmlCharField(
        verbose_name='Имя', max_length=32, blank=False
    )
    phone = SafeHtmlCharField(
        verbose_name='Телефон', max_length=26,
        validators=[phone_regex], blank=True, default=''
    )
    created_dt = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True, db_index=True)
    is_active = models.BooleanField(default=False, verbose_name='Отображено на сайте', db_index=True)
    content = SafeHtmlTextField(blank=False, verbose_name='Текст отзыва')

    objects = BaseQuerySet.as_manager()

    class Meta:
        abstract = True


class SiteReview(BaseReviewModel):

    class Meta:
        verbose_name = 'Отзыв на сайте'
        verbose_name_plural = 'Отзывы на сайте'


class Review(BaseReviewModel):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = 'Отзыв на модель'
        verbose_name_plural = 'Отзывы на модель'
