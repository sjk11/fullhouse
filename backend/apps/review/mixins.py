from django.db import models

from django.contrib.contenttypes.fields import GenericRelation
from backend.apps.review import models as review_models


class ReviewMixin(models.Model):
    reviews = GenericRelation(review_models.Review)

    class Meta:
        abstract = True
