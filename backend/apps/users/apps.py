from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'backend.apps.users'

    verbose_name = 'Пользователь'
    verbose_name_plural = 'Пользователи'

    def ready(self):
        from . import signals
