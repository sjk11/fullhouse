from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline

from backend.apps.seo import models as seo_models


class BaseReviewAdmin(admin.ModelAdmin):
    list_display = ['title', 'keyword', 'description']
    search_fields = ['title', 'keyword', 'description']


@admin.register(seo_models.SeoData)
class SeoAdmin(BaseReviewAdmin):
    pass


class SeoDataInlineAdmin(GenericStackedInline):
    model = seo_models.SeoData

    def get_extra(self, request, obj=None, **kwargs):
        if obj is not None and obj.seodata.all():
            return 0
        return 1
