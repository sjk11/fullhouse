from django.db import models

from django.contrib.contenttypes.fields import GenericRelation
from backend.apps.seo import models as seo_models


class SeoDataMixin(models.Model):
    seodata = GenericRelation(seo_models.SeoData)

    class Meta:
        abstract = True
