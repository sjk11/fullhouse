from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site

from django.db import models

from backend.base.fields import SafeHtmlTextField, SafeHtmlCharField


class BaseSeoModel(models.Model):
    title = SafeHtmlCharField(
        verbose_name='Заголовок', max_length=32, blank=False
    )
    keyword = SafeHtmlCharField(
        verbose_name='Ключевые слова', max_length=72,
        blank=False
    )
    description = SafeHtmlTextField(blank=False, verbose_name='Описание')

    class Meta:
        abstract = True


class SeoData(BaseSeoModel):
    og_title = models.CharField(verbose_name='og title', max_length=200, default='', blank=True)
    og_description = models.CharField(verbose_name='og description', max_length=256, default='', blank=True)
    og_type = models.CharField(verbose_name='og type', max_length=64, default='website',
                               choices=(('website', 'website'),
                                        ('product', 'product'),
                                        ('article', 'article'),
                                        ))
    og_image = models.CharField(verbose_name='og image', max_length=512, default='', blank=True)
    og_url = models.CharField(verbose_name='og url', max_length=512, default='', blank=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = 'Seo Описание'
        verbose_name_plural = 'Seo Описание'
        unique_together = (('content_type', 'object_id'),)

    def get_seo_data(self):
        data = {}
        for field in ['title', 'keyword', 'description', 'og_title', 'og_description', 'og_type', 'og_image', 'og_url']:
            v = getattr(self, field)
            if not v:
                continue
            data[field] = v
        return data
