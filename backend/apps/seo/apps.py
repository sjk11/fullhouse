from django.apps import AppConfig


class SeoConfig(AppConfig):
    name = 'backend.apps.seo'
    verbose_name = 'СЕО данные '
    verbose_name_plural = 'СЕО данные '
